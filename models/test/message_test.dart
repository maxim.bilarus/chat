import 'package:test/test.dart';
import 'package:models/models.dart';

void main() {
  final profile = Profile(
    name: 'name',
    surname: 'surname',
    email: 'email',
  );
  final profileJson = {
    'name': 'name',
    'surname': 'surname',
    'email': 'email',
  };
  final user = User(
    id: UserId('id'),
    login: 'login',
    password: 'password',
    profile: profile,
  );

  final userJson = {
    'id': 'id',
    'login': 'login',
    'password': 'password',
    'profile': profileJson,
  };
  final message = Message(
    id: MessageId('id'),
    text: 'text',
    author: user,
  );
  final messageJson = {
    'id': 'id',
    'text': 'text',
    'author': userJson,
  };
  group('create MessageId', () {
    test('from null', () {
      expect(MessageId(null), isNull);
    });

    test('from not null id', () {
      final id = MessageId('id');
      expect(id.json, 'id');
    });
  });

  group('Message', () {
    test('create from null', () {
      expect(Message.fromJson(null), isNull);
    });
    test('create from not null', () {
      final message1 = Message.fromJson(messageJson);
      expect(
          message1.json..removeWhere((k, v) => v.runtimeType == DateTime),
          equals(
              message.json..removeWhere((k, v) => v.runtimeType == DateTime)));
    });

    test('to json', () {
      expect(
          message.json..removeWhere((k, v) => v.runtimeType == DateTime),
          equals(
              messageJson..removeWhere((k, v) => v.runtimeType == DateTime)));
    });
  });
}

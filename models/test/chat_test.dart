import 'package:test/test.dart';
import 'package:models/models.dart';

void main() {
  final profile = Profile(
    name: 'name',
    surname: 'surname',
    email: 'email',
  );
  final profileJson = {
    'name': 'name',
    'surname': 'surname',
    'email': 'email',
  };
  final user = User(
    id: UserId('id'),
    login: 'login',
    password: 'password',
    profile: profile,
  );

  final userJson = {
    'id': 'id',
    'login': 'login',
    'password': 'password',
    'profile': profileJson,
  };

  final chat = Chat(
    id: ChatId('id'),
    title: 'title',
    creator: user,
    members: [user],
  );

  final chatJson = {
    'id': 'id',
    'title': 'title',
    'creator': userJson,
    'members': [userJson]
  };
  group('create ChatId', () {
    test('from null', () {
      expect(ChatId(null), isNull);
    });

    test('from not null id', () {
      final id = ChatId('id');
      expect(id.json, 'id');
    });
  });

  group('Chat', () {
    test('create from null', () {
      expect(Chat.fromJson(null), isNull);
    });
    test('create from not null', () {
      final chat1 = Chat.fromJson(chatJson);
      expect(chat1.json..removeWhere((k, v) => v.runtimeType == DateTime),
          equals(chat.json..removeWhere((k, v) => v.runtimeType == DateTime)));
    });

    test('to json', () {
      expect(chat.json..removeWhere((k, v) => v.runtimeType == DateTime),
          equals(chatJson..removeWhere((k, v) => v.runtimeType == DateTime)));
    });
  });
}

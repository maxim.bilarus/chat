import 'package:test/test.dart';
import 'package:models/models.dart';

void main() {
  final profile = Profile(
      name: 'name',
      surname: 'surname',
      patronymic: 'patronymic',
      email: 'email',
      birthday: DateTime(2002, 6, 27));
  final profileJson = {
    'name': 'name',
    'surname': 'surname',
    'patronymic': 'patronymic',
    'email': 'email',
    'birthday': '2002-06-27'
  };
  final user = User(
      id: UserId('id'), login: 'login', password: 'password', profile: profile);

  final userJson = {
    'id': 'id',
    'login': 'login',
    'password': 'password',
    'profile': profileJson
  };
  group('create UserId', () {
    test('from null id', () {
      expect(UserId(null), isNull);
    });

    test('from not null id', () {
      final id = UserId('id');
      expect(id.json, 'id');
    });
  });

  group('User', () {
    test('create from null', () {
      expect(User.fromJson(null), isNull);
    });

    test('create from not null', () {
      final user1 = User.fromJson(userJson);
      expect(user1, equals(user));
    });

    test('to json', () {
      expect(user.json, equals(userJson));
    });
  });

  group('Profile', () {
    test('create from null', () {
      expect(Profile.fromJson(null), isNull);
    });
    test('create from not null', () {
      final profile1 = Profile.fromJson(profileJson);
      expect(profile1, equals(profile));
    });

    test('to json', () {
      expect(profile.json, equals(profileJson));
    });
  });
}

import 'package:equatable/equatable.dart';
import 'package:data_model/data_model.dart';
import 'package:models/models.dart';

void main() {}

/// Идентификатор сообщения
class MessageId extends ObjectId {
  MessageId._(id) : super(id);

  factory MessageId(id) {
    if (id == null) return null;
    return MessageId._(id);
  }
}

/// Сообщение
class Message with EquatableMixin implements Model<MessageId> {
  /// Идентификатор
  @override
  MessageId id;

  /// Текст
  String text;

  /// Отправитель
  User author;

  /// Чат в который отправленно сообщение
  ChatId chatId;

  /// Дата сообщения
  DateTime datetime;

  Message({this.id, this.text, this.author, this.chatId, this.datetime});

  factory Message.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return Message(
      id: MessageId(json['id']),
      text: json['text'],
      author: User.fromJson(json['author']),
      chatId: ChatId(json['chatId']),
      datetime: json['datetime'],
    );
  }

  @override
  Map<String, dynamic> get json => {
        'id': id?.json,
        'text': text,
        'author': author?.json,
        'chatId': chatId?.json,
        'datetime': datetime,
      }..removeWhere((k, v) => v == null);

  @override
  List<Object> get props => [
        id,
        text,
        author,
        chatId,
        datetime,
      ];
}

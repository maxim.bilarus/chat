import 'package:equatable/equatable.dart';
import 'package:data_model/data_model.dart';
import 'package:intl/intl.dart';

/// Идентификатор пользователя
class UserId extends ObjectId {
  UserId._(id) : super(id);

  factory UserId(id) {
    if (id == null) return null;
    return UserId._(id);
  }
}

/// Пользователь
class User with EquatableMixin implements Model<UserId> {
  /// Идентификатор
  @override
  UserId id;

  /// Логин
  String login;

  /// Пароль
  String password;

  /// Профиль
  Profile profile;

  /// Свойство блокировки пользователя
  bool deleted;

  User({this.id, this.login, this.password, this.profile, this.deleted});

  factory User.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return User(
        id: UserId(json['id']),
        login: json['login'],
        password: json['password'],
        profile: Profile.fromJson(json['profile']),
        deleted: json['deleted']);
  }

  @override
  Map<String, dynamic> get json => {
        'id': id?.json,
        'login': login,
        'password': password,
        'profile': profile?.json,
        'deleted': deleted,
      }..removeWhere((k, v) => v == null);

  @override
  List<Object> get props => [
        id,
        login,
        password,
        profile,
        deleted,
      ];
}

/// Профиль пользователя
class Profile with EquatableMixin implements JsonEncodable {
  /// Имя
  String name;

  /// Фамилия
  String surname;

  /// Отчество
  String patronymic;

  /// Почта
  String email;

  /// Дата рождения
  DateTime birthday;

  Profile({
    this.name,
    this.surname,
    this.patronymic,
    this.email,
    this.birthday,
  });
  factory Profile.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return Profile(
        name: json['name'],
        surname: json['surname'],
        patronymic: json['patronymic'],
        email: json['email'],
        birthday:
            json['birthday'] != null ? DateTime.parse(json['birthday']) : null);
  }

  @override
  Map<String, dynamic> get json => {
        'name': name,
        'surname': surname,
        'patronymic': patronymic,
        'email': email,
        'birthday':
            birthday != null ? DateFormat('yyyy-MM-dd').format(birthday) : null
      }..removeWhere((k, v) => v == null);

  @override
  List<Object> get props => [
        name,
        surname,
        patronymic,
        email,
        birthday,
      ];
}

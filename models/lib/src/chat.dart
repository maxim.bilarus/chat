import 'package:equatable/equatable.dart';
import 'package:data_model/data_model.dart';
import 'package:models/models.dart';

/// Идентификатор чата
class ChatId extends ObjectId {
  ChatId._(id) : super(id);

  factory ChatId(id) {
    if (id == null) return null;
    return ChatId._(id);
  }
}

/// Чат
class Chat with EquatableMixin implements Model<ChatId> {
  /// Идентификатор
  @override
  ChatId id;

  /// Название чата
  String title;

  /// Создатель чата
  User creator;

  /// Список пользователей
  List<User> members;

  Chat({this.id, this.title, this.creator, this.members});

  factory Chat.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return Chat(
      id: ChatId(json['id']),
      title: json['title'],
      creator: User.fromJson(json['creator']),
      members: json['members'].map<User>((u) => User.fromJson(u)).toList(),
    );
  }

  @override
  Map<String, dynamic> get json => {
        'id': id?.json,
        'title': title,
        'creator': creator?.json,
        'members': members.map((member) => member.json).toList(),
      }..removeWhere((k, v) => v == null);

  @override
  List<Object> get props => [
        id,
        title,
        creator,
        members,
      ];
}

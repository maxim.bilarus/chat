import 'dart:convert';

import 'package:mongo_dart/mongo_dart.dart';
import 'package:api/collections.dart';

void main() async {
  Db db = Db('mongodb://127.0.0.1/chat');
  DbCollection collection = DbCollection(db, 'users');
  UsersCollection usersCollection = UsersCollection(collection);
  await db.open();
  final u = await usersCollection
      .find(where.oneFrom('profile.name', ['Maxim', 'Vasiliy', 'Petya']))
      .toList();
  print(u);
  String s = 'abc';
  String b = r'^' + s;
  print(b);
  await db.close();
  DateTime t = DateTime(2019, 11, 30);
  print(t);
  print(6 ~/ 5);
  List list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  print(list.take(5).toList());
  final JsonEncoder js = JsonEncoder();
}

import 'package:models/models.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:rest_api_server/mongo_collection.dart';

/// Коллекция сообщений
class MessagesCollection extends MongoCollection<Message, MessageId> {
  MessagesCollection(DbCollection collection) : super(collection);

  /// Создает модель сообщения из json
  @override
  Message createModel(Map<String, dynamic> data) => Message.fromJson(data
    ..addAll({'id': MessageId(data['_id'].toHexString())})
    ..remove('_id')
    ..update('chatId', (chatId) => ChatId(chatId.toHexString()))
    ..update(
        'author',
        (author) => author
          ..addAll({'id': UserId(author['_id'].toHexString())})
          ..remove('_id')));

  /// Создает пайплайн для коллекции сообщений
  @override
  List<Map<String, dynamic>> buildPipeline(SelectorBuilder query) {
    final pipeline = AggregationPipelineBuilder()
        .addStage(Match(query.map['\$query']))
        .addStage(Lookup(
            from: 'users',
            localField: 'author',
            foreignField: '_id',
            as: 'author'))
        .addStage(Unwind(Field('author')));

    if (query.map.containsKey('orderby')) {
      pipeline.addStage(Sort(query.map['orderby']));
    }
    if (query.paramSkip != 0) pipeline.addStage(Skip(query.paramSkip));
    if (query.paramLimit != 0) pipeline.addStage(Limit(query.paramLimit));
    return pipeline.build();
  }

  /// Создает документ сообщения в коллекции
  Future<Message> createMessage(Message mess) async {
    final id = ObjectId();
    final messJson = mess.json
      ..addAll({'_id': id})
      ..remove('id')
      ..update('chatId', (chatId) => ObjectId.fromHexString(chatId))
      ..update('author', (author) => ObjectId.fromHexString(author['id']));
    await collection.insert(messJson);
    return getObjectById(id);
  }

  /// Обновляет документ сообщения в коллекции
  Future<Message> updateMessage(Message mess) async {
    final mongoId = ObjectId.fromHexString(mess.id.value);
    final messJson = mess.json
      ..update('chatId', (chatId) => ObjectId.fromHexString(chatId))
      ..update('author', (author) => ObjectId.fromHexString(author['id']))
      ..remove('id')
      ..addAll({'_id': mongoId});
    await collection
        .update(where.eq('_id', mongoId), {'\$set': messJson..remove('id')});
    return getObjectById(mongoId);
  }

  @override
  Future<Message> update(Message _) {
    throw UnsupportedError('method not appliable');
  }

  @override
  Future<Message> replace(Message _) {
    throw UnsupportedError('method not appliable');
  }
}

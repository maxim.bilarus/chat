import 'package:models/models.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:rest_api_server/mongo_collection.dart';

/// Коллекция чатов
class ChatsCollection extends MongoCollection<Chat, ChatId> {
  ChatsCollection(DbCollection collection) : super(collection);

  /// Создает модель чата из json
  @override
  Chat createModel(Map<String, dynamic> data) => Chat.fromJson(data
    ..addAll({'id': data['_id'].toHexString()})
    ..remove('_id')
    ..update(
        'members',
        (members) => (members as List)
            .map((m) => m
              ..addAll({'id': m['_id'].toHexString()})
              ..remove('_id'))
            .toList())
    ..update(
        'creator',
        (creator) => creator
          ..addAll({'id': creator['_id'].toHexString()})
          ..remove('_id')));

  /// Создает пайплайн для коллекции чатов
  @override
  List<Map<String, dynamic>> buildPipeline(SelectorBuilder query) {
    final pipeline = AggregationPipelineBuilder()
        .addStage(Match(query.map['\$query']))
        .addStage(Lookup(
            from: 'users',
            localField: 'creator',
            foreignField: '_id',
            as: 'creator'))
        .addStage(Lookup(
            from: 'users',
            localField: 'members',
            foreignField: '_id',
            as: 'members'))
        .addStage(Unwind(Field('creator')));

    if (query.map.containsKey('orderby')) {
      pipeline.addStage(Sort(query.map['orderby']));
    }
    if (query.paramSkip != 0) pipeline.addStage(Skip(query.paramSkip));
    if (query.paramLimit != 0) pipeline.addStage(Limit(query.paramLimit));
    return pipeline.build();
  }

  /// Создает документ чата в коллекции
  Future<Chat> createChat(Chat chat) async {
    final id = ObjectId();
    final chatJson = chat.json
      ..update(
          'members',
          (members) => (members as List)
              .map((m) => ObjectId.fromHexString((m
                ..addAll({'_id': m['id']})
                ..remove('id'))['_id']))
              .toList())
      ..update(
          'creator',
          (creator) => ObjectId.fromHexString((creator
            ..addAll({'_id': creator['id']})
            ..remove('id'))['_id']))
      ..remove('id')
      ..addAll({'_id': id});
    await collection.insert(chatJson);
    return getObjectById(id);
  }

  /// Обновляет документ чата в коллекции
  Future<Chat> updateChat(Chat chat) async {
    final mongoId = ObjectId.fromHexString(chat.id.json);
    await collection.update(where.eq('_id', mongoId), {
      '\$set': chat.json
        ..remove('id')
        ..update(
            'members',
            (members) => (members as List)
                .map((m) => ObjectId.fromHexString((m
                  ..addAll({'_id': m['id']})
                  ..remove('id'))['_id']))
                .toList())
        ..update(
            'creator',
            (creator) => ObjectId.fromHexString((creator
              ..addAll({'_id': creator['id']})
              ..remove('id'))['_id']))
    });
    return getObjectById(mongoId);
  }

  @override
  Future<Chat> delete(ChatId _) {
    throw UnsupportedError('method not appliable');
  }

  @override
  Future<Chat> update(Chat _) {
    throw UnsupportedError('method not appliable');
  }

  @override
  Future<Chat> replace(Chat _) {
    throw UnsupportedError('method not appliable');
  }

  @override
  Future<Chat> insert(Chat object) async {
    throw UnsupportedError('method not appliable');
  }
}

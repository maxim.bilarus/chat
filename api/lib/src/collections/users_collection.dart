import 'package:models/models.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:rest_api_server/mongo_collection.dart';

/// Коллекция пользователей
class UsersCollection extends MongoCollection<User, UserId> {
  UsersCollection(mongo.DbCollection collection) : super(collection);

  /// Создание модели пользователя из json
  @override
  User createModel(Map<String, dynamic> data) => User.fromJson(data);
}

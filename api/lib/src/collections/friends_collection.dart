import 'package:models/models.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:rest_api_server/mongo_collection.dart';
import 'package:stream_transform/stream_transform.dart';

/// Коллекция друзей
class FriendsCollection extends MongoCollection<User, UserId> {
  FriendsCollection(DbCollection collection) : super(collection);

  /// Создает модель пользователя из json
  @override
  User createModel(Map<String, dynamic> data) => User.fromJson(data
    ..addAll({'id': UserId(data['_id'].value.toHexString())})
    ..remove('_id'));

  /// Строит пайплайн для коллекции друзей
  @override
  List<Map<String, dynamic>> buildPipeline(SelectorBuilder query) {
    final pipeline = AggregationPipelineBuilder()
        .addStage(Match(query.map['\$query']))
        .addStage(Lookup(
            from: 'users',
            localField: 'friend',
            foreignField: '_id',
            as: 'friend'))
        .addStage(Unwind(Field('friend')))
        .addStage(ReplaceRoot(Field('friend')));
    if (query.map.containsKey('orderby')) {
      pipeline.addStage(Sort(query.map['orderby']));
    }
    if (query.paramSkip != 0) pipeline.addStage(Skip(query.paramSkip));
    if (query.paramLimit != 0) pipeline.addStage(Limit(query.paramLimit));
    return pipeline.build();
  }

  /// Ищет документы друзей в коллекции по id
  Stream<User> findFriends(UserId id) async* {
    final friends = ((await getObjectsByQuery(
            where.eq('user', ObjectId.fromHexString(id.value)))
        .toList())
      ..removeWhere((f) => f.id == id));
    yield* Stream.fromIterable(friends);
  }

  Stream<User> findFriendsOfFriends(UserId id) async* {
    final friends = (await findFriends(id).toList())
      ..removeWhere((u) => u.id == id);
    final friendsIdentifiers = friends
        .map((f) => ObjectId.fromHexString(f.id.value.toString()))
        .toList();
    final friendsOfFriend =
        ((await getObjectsByQuery(where.oneFrom('user', friendsIdentifiers))
                    .toList())
                .map((u) => u.json)
                .toList()
                  ..remove(friends))
            .map((u) => User.fromJson(u))
            .toList();
    yield* Stream.fromIterable(
        (friendsOfFriend..removeWhere((u) => u.id == id)).toList());
  }

  ///Удаляет документы друзей в коллекции
  Stream<User> deleteFriend(UserId user, UserId friend) {
    collection.remove(where
        .eq('user', ObjectId.fromHexString(user.value))
        .eq('friend', ObjectId.fromHexString(friend.value)));
    collection.remove(where
        .eq('user', ObjectId.fromHexString(friend.value))
        .eq('friend', ObjectId.fromHexString(user.value)));

    return findFriends(UserId(user.value));
  }

  /// Добавляет документы друзей в коллекцию
  Stream<User> addFriend(UserId user, UserId friend) {
    collection.insertAll([
      {
        'user': ObjectId.fromHexString(user.value),
        'friend': ObjectId.fromHexString(friend.value)
      },
      {
        'user': ObjectId.fromHexString(friend.value),
        'friend': ObjectId.fromHexString(user.value)
      }
    ]);
    return findFriends(UserId(user.value));
  }

  @override
  Future<User> delete(UserId _) {
    throw UnsupportedError('method not appliable');
  }

  @override
  Future<User> update(User _) {
    throw UnsupportedError('method not appliable');
  }

  @override
  Future<User> replace(User _) {
    throw UnsupportedError('method not appliable');
  }

  @override
  Future<User> insert(User _) {
    throw UnsupportedError('method not appliable');
  }

  @override
  Future<User> findOne(UserId _) {
    throw UnsupportedError('method not appliable');
  }

  @override
  Stream<User> find([SelectorBuilder _]) {
    throw UnsupportedError('method not appliable');
  }
}

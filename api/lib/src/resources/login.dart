import 'package:api/collections.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'dart:io';
import 'dart:convert';
import 'package:mongo_dart/mongo_dart.dart' as mongo;

/// Ресурс входа в систему
@Resource(path: 'login')
class Login {
  final UsersCollection usersCollection = locateService<UsersCollection>();

  /// Выполняет вход пользователя в систему
  @Post()
  Future<shelf.Response> login(Map requestBody) async {
    var foundUsers = await usersCollection
        .find(mongo.where
            .eq('login', requestBody['login'])
            .eq('password', requestBody['password']))
        .toList();
    if (foundUsers.isEmpty) {
    } else {
      final user = foundUsers.single;
      return shelf.Response.ok(
          json.encode(user.json..remove('password'), toEncodable: _toEncodable),
          headers: {'Content-Type': ContentType.json.toString()},
          context: {'subject': user.id.json, 'payload': user.json});
    }
  }
}

String _toEncodable(value) {
  if (value is DateTime) return value.toUtc().toIso8601String();
  return '';
}

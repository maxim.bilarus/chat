import 'package:models/models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'package:api/collections.dart';

/// Ресурс чатов
@Resource(path: 'chats')
class Chats {
  final UsersCollection usersCollection = locateService<UsersCollection>();

  final ChatsCollection chatsCollection = locateService<ChatsCollection>();
  final MessagesCollection messagesCollection =
      locateService<MessagesCollection>();

  /// Создает чат
  @Post()
  Future<Chat> create(Map requestBody, Map context) async {
    if (requestBody == null) {
      throw NotFoundException();
    }
    final chat = await chatsCollection.createChat(Chat.fromJson(requestBody));
    if (chat == null) {
      throw NotFoundException();
    }
    return chat;
  }

  /// Добавляет собеседника в чат
  @Post(path: '{id}/members')
  Future<List<User>> addMember(String id, Map requestBody, Map context) async {
    final member = await usersCollection.findOne(UserId(requestBody['id']));
    final chat = await chatsCollection.findOne(ChatId(id));
    if (chat == null || member == null) {
      throw NotFoundException();
    }
    final List<Map<String, dynamic>> members1 = (chat.json['members'] as List)
      ..add(member.json);
    final chatJson = chat.json..update('members', (members) => members1);
    final chatWithNewMember =
        await chatsCollection.updateChat(Chat.fromJson(chatJson));
    return chatWithNewMember.members;
  }

  /// Удаляет собеседника из чата
  @Delete(path: '{idChat}/members/{idMember}')
  Future<List<User>> deleteMember(
      String idChat, String idMember, Map context) async {
    final chat = await chatsCollection.findOne(ChatId(idChat));
    final member = await usersCollection.findOne(UserId(idMember));
    if (chat == null || member == null) {
      throw NotFoundException();
    }
    final Map<String, dynamic> chatJson = chat.json
      ..['members'].removeWhere((v) => v['id'] == idMember);
    final chatWithoutDeletedMember =
        await chatsCollection.updateChat(Chat.fromJson(chatJson));
    return chatWithoutDeletedMember.members;
  }

  /// Ищет чаты по контексту
  @Get()
  Future<List<Chat>> find(Map context,
      [String memberId,
      String creatorId,
      String title,
      int startNumberOfMembers,
      int endNumberOfMembers]) async {
    List<Chat> chats = await chatsCollection
        .find((memberId != null
                ? where.eq('members', ObjectId.fromHexString(memberId))
                : where)
            .and((creatorId != null
                ? where.eq('author', ObjectId.fromHexString(creatorId))
                : where))
            .and((title != null ? where.eq('title', title) : where)))
        ?.toList();
    if (startNumberOfMembers != null) {
      chats.removeWhere((c) => c.members.length < startNumberOfMembers);
    }
    if (endNumberOfMembers != null) {
      chats.removeWhere((c) => c.members.length > endNumberOfMembers);
    }
    return chats;
  }

  /// Ищет чаты по id
  @Get(path: '{id}')
  Future<Chat> findChat(String id, Map context) async {
    final chat = await chatsCollection.findOne(ChatId(id));
    if (chat == null) {
      throw NotFoundException();
    }
    return chat;
  }

  /// Обновляет чат
  @Patch(path: '{id}')
  Future<Chat> update(String id, Map requestBody, Map context) async {
    if (requestBody == null) {
      throw NotFoundException();
    }
    final chat = await chatsCollection.findOne(ChatId(id));
    if (chat == null) {
      throw NotFoundException();
    }
    final updateChat = await chatsCollection.updateChat(Chat.fromJson(
        (requestBody as Map<String, dynamic>)..addAll({'id': id})));
    return updateChat;
  }
}

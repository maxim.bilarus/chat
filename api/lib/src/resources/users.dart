import 'dart:convert';

import 'package:models/models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'package:api/collections.dart';

/// Ресурс пользователей
@Resource(path: 'users')
class Users {
  final UsersCollection usersCollection = locateService<UsersCollection>();
  final FriendsCollection friendsCollection =
      locateService<FriendsCollection>();

  /// Создает пользователя
  @Post()
  Future<User> create(Map requestBody) async {
    final user = await usersCollection.insert(User.fromJson(requestBody));
    if (user == null) {
      throw NotFoundException();
    }
    return user;
  }

  /// Ищет пользователя по id
  @Get(path: '{id}')
  Future<User> findUser(String id) async {
    final user = await usersCollection.findOne(UserId(id));
    if (user == null) {
      throw NotFoundException();
    }
    return user;
  }

  /// Ищет пользователей по поисковой строке
  @Get()
  Future<List<User>> find(Map context,
      [String searchString,
      String patronymic,
      String gender,
      int yearsOldStart,
      int yearsOldEnd]) async {
    RegExp reg1 = RegExp(r'^[A-zА-яЁё]+\s[A-zА-яЁё]+$');
    RegExp reg2 = RegExp(r'^[A-zА-яЁё]+$');
    RegExp reg4 = RegExp(r'[A-zА-яЁё]+');
    RegExp reg3 = RegExp(r'\s[A-zА-яЁё]+');
    List<User> users;
    if (searchString == null) {
      users = await usersCollection.find(where).toList();
    } else if (searchString != null && reg1.hasMatch(searchString)) {
      RegExpMatch firstMatch = reg4.firstMatch(searchString);
      RegExpMatch secondMatch = reg3.firstMatch(searchString);
      String string1 =
          searchString.substring(firstMatch.start, firstMatch.end).toString();
      String string2 = reg4.stringMatch(searchString
          .substring(secondMatch.start, secondMatch.end)
          .toString());

      users = await usersCollection
          .find((where
                  .match('profile.name', r'^' + string1, caseInsensitive: true)
                  .and(where.match('profile.surname', r'^' + string2,
                      caseInsensitive: true)))
              .or(where
                  .match('profile.name', r'^' + string2, caseInsensitive: true)
                  .and(where.match('profile.surname', r'^' + string1,
                      caseInsensitive: true))))
          .toList();
    } else if (searchString != null && reg2.hasMatch(searchString)) {
      users = await usersCollection
          .find((where
              .match('profile.name', r'^' + searchString, caseInsensitive: true)
              .or(where.match('profile.surname', r'^' + searchString,
                  caseInsensitive: true))))
          .toList();
    }
    if (patronymic != null) {
      users.removeWhere((u) => u?.profile?.patronymic != null
          ? u.profile.patronymic != patronymic
          : u == u);
    }
    if (yearsOldStart != null) {
      users.removeWhere((u) => (u?.profile?.birthday != null
          ? -(u.profile.birthday.difference(DateTime.now())).inDays ~/ 365 <
              yearsOldStart
          : u == u));
    }
    if (yearsOldEnd != null) {
      users.removeWhere((u) => (u?.profile?.birthday != null
          ? -(u.profile.birthday.difference(DateTime.now())).inDays ~/ 365 >
              yearsOldEnd
          : u == u));
    }
    return users;
  }

  /// Ищет друзей пользователя по id пользователя
  @Get(path: '{id}/friends')
  Future<List<User>> findFriends(String id, Map context,
      [String searchString,
      String patronymic,
      int yearsOldStart,
      int yearsOldEnd,
      bool friendsOfFriend]) async {
    RegExp reg1 = RegExp(r'^[A-zА-яЁё]+\s[A-zА-яЁё]+$');
    RegExp reg2 = RegExp(r'^[A-zА-яЁё]+$');
    RegExp reg4 = RegExp(r'[A-zА-яЁё]+');
    RegExp reg3 = RegExp(r'\s[A-zА-яЁё]+');
    List<User> friends;

    List<User> allFriends;
    if (friendsOfFriend == true) {
      allFriends =
          await friendsCollection.findFriendsOfFriends(UserId(id)).toList();
    } else {
      allFriends = (await friendsCollection.findFriends(UserId(id)).toList())
        ..removeWhere((f) => f.id.value == id);
    }
    if (searchString == null) {
      friends = allFriends;
    } else if (searchString != null && reg1.hasMatch(searchString)) {
      RegExpMatch firstMatch = reg4.firstMatch(searchString);
      RegExpMatch secondMatch = reg3.firstMatch(searchString);
      String string1 =
          searchString.substring(firstMatch.start, firstMatch.end).toString();
      String string2 = reg4.stringMatch(searchString
          .substring(secondMatch.start, secondMatch.end)
          .toString());
      RegExp nameRegExp =
          RegExp(r'^' + string1[0].toUpperCase() + string1.substring(1));
      RegExp surnameRegExp =
          RegExp(r'^' + string2[0].toUpperCase() + string2.substring(1));
      friends = allFriends
        ..removeWhere((f) => !((nameRegExp.hasMatch(f.profile.name) &&
                surnameRegExp.hasMatch(f.profile.surname)) ||
            (nameRegExp.hasMatch(f.profile.surname) &&
                surnameRegExp.hasMatch(f.profile.name))));
    } else if (searchString != null && reg2.hasMatch(searchString)) {
      RegExp nameRegExp = RegExp(
          r'^' + searchString[0].toUpperCase() + searchString.substring(1));
      RegExp surnameRegExp = RegExp(
          r'^' + searchString[0].toUpperCase() + searchString.substring(1));
      friends = allFriends
        ..removeWhere((f) => !((nameRegExp.hasMatch(f.profile.name) ||
            (surnameRegExp.hasMatch(f.profile.surname)))));
    }
    if (patronymic != null && friends.isNotEmpty) {
      friends.removeWhere((f) => f?.profile?.patronymic != null
          ? f.profile.patronymic != patronymic
          : f == f);
    }
    if (yearsOldStart != null && friends.isNotEmpty) {
      friends.removeWhere((f) => (f?.profile?.birthday != null
          ? -(f.profile.birthday.difference(DateTime.now())).inDays ~/ 365 <
              yearsOldStart
          : f == f));
    }
    if (yearsOldEnd != null && friends.isNotEmpty) {
      friends.removeWhere((f) => (f?.profile?.birthday != null
          ? -(f.profile.birthday.difference(DateTime.now())).inDays ~/ 365 >
              yearsOldEnd
          : f == f));
    }
    return friends;
  }

  /// Добавляет пользователя в друзья
  @Post(path: '{id}/friends')
  Future<List<User>> addFriend(String id, Map requestBody, Map context) async {
    final user = await usersCollection.findOne(UserId(id));
    final friend = await usersCollection.findOne(UserId(requestBody['id']));
    if (user == null || friend == null) {
      throw NotFoundException();
    }
    final friends = await friendsCollection
        .addFriend(UserId(id), UserId(requestBody['id']))
        .toList();
    return friends;
  }

  /// Удаляет пользователя из друзей
  @Delete(path: '{id}/friends')
  Future<List<User>> deleteFriend(
      String id, Map requestBody, Map context) async {
    final user = await usersCollection.findOne(UserId(id));
    final deleteFriend =
        await usersCollection.findOne(UserId(requestBody['id']));
    if (user == null || deleteFriend == null) {
      throw NotFoundException();
    }
    final friends = await friendsCollection
        .deleteFriend(UserId(id), UserId(requestBody['id']))
        ?.toList();
    return friends;
  }

  /// Обновляет пользователя
  @Patch(path: '{id}')
  Future<User> update(String id, Map requestBody, Map context) async {
    final user = await usersCollection.findOne(UserId(id));
    if (user == null) {
      throw NotFoundException();
    }
    final updateUser = await usersCollection.update(User.fromJson(
        (requestBody as Map<String, dynamic>)..addAll({'id': id})));
    return updateUser;
  }

  /// Блокирует пользователя
  @Patch(path: '{id}/block')
  Future<User> blockUser(String id) async {
    final searchuser = await usersCollection.findOne(UserId(id));
    if (searchuser == null) {
      throw NotFoundException();
    }
    final user = await usersCollection
        .update(User.fromJson(searchuser.json..addAll({'deleted': true})));
    return user;
  }
}

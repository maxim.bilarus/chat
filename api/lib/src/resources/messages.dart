import 'package:models/models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:api/collections.dart';

/// Ресурс сообщений
@Resource(path: 'messages')
class Messages {
  final ChatsCollection chatsCollection = locateService<ChatsCollection>();
  final MessagesCollection messagesCollection =
      locateService<MessagesCollection>();

  /// Создает сообщение
  @Post()
  Future<Message> create(Map requestBody) async {
    final message =
        await messagesCollection.createMessage(Message.fromJson(requestBody));
    if (message == null) {
      throw NotFoundException();
    }
    return message;
  }

  /// Ищет сообщения по контексту
  @Get()
  Future<List<Message>> find(Map<String, dynamic> context,
      [String authorId, String chatId]) async {
    List<Message> messages;
    if (authorId?.trim()?.isEmpty == false &&
        authorId?.trim()?.isEmpty != null) {
      messages = await messagesCollection
          .find(where.eq('author', ObjectId.fromHexString(chatId)))
          ?.toList();
    }
    if (chatId?.trim()?.isEmpty == false && chatId?.trim()?.isEmpty != null) {
      messages = await messagesCollection
          .find(where.eq('chatId', ObjectId.fromHexString(chatId)))
          ?.toList();
    } else if ((authorId?.trim()?.isEmpty == true ||
            authorId?.trim()?.isEmpty == null) &&
        (chatId?.trim()?.isEmpty == true || chatId?.trim()?.isEmpty == null)) {
      messages = await messagesCollection
          .find(where.eq('author', ObjectId.fromHexString(context['subject'])))
          ?.toList();
    }
    return messages;
  }

  /// Ищет сообщения по id
  @Get(path: '{id}')
  Future<Message> findMessage(String id, Map context) async {
    final message = await messagesCollection.findOne(MessageId(id));
    if (message == null) {
      throw NotFoundException();
    }
    return message;
  }

  /// Удаляет сообщение по id
  @Delete(path: '{idChat}/messages/{idMessage}')
  Future<List<Message>> deleteMessage(String idMessage, Map context) async {
    final message = await messagesCollection.findOne(MessageId(idMessage));
    final idChat = message?.chatId?.value;
    final chat = await chatsCollection.findOne(ChatId(idChat));
    if (message == null || chat == null) {
      throw NotFoundException();
    }
    await messagesCollection.delete(message.id);
    final messages = await messagesCollection
        .find(where.eq('chatId', ObjectId.fromHexString(idChat)))
        ?.toList();
    return messages;
  }

  /// Редактирует сообщение
  @Patch(path: '{id}')
  Future<Message> editMessage(String id, Map requestBody, Map context) async {
    if (requestBody == null) {
      throw NotFoundException();
    }
    final message = messagesCollection.findOne(MessageId(id));
    if (message == null) {
      throw NotFoundException();
    }
    final updateMessage = await messagesCollection.updateMessage(
        Message.fromJson(
            (requestBody as Map<String, dynamic>)..addAll({'id': id})));
    return updateMessage;
  }
}

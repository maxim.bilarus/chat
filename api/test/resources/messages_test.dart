import 'package:api/collections.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import 'package:api/src/resources/messages.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'package:rest_api_server/src/service_registry.dart';
import 'package:get_it/get_it.dart';
import 'testModels.dart';

class MessagesCollectionMock extends Mock implements MessagesCollection {}

class ChatsCollectionMock extends Mock implements ChatsCollection {}

void main() {
  MessagesCollection messagesCollection;
  Messages messagesResource;
  ChatsCollection chatsCollection;
  setUp(() {
    messagesCollection = MessagesCollectionMock();
    chatsCollection = ChatsCollectionMock();
    register<MessagesCollection>(messagesCollection);
    register<ChatsCollection>(chatsCollection);
    messagesResource = Messages();
  });
  tearDown(() {
    GetIt.I.unregister<MessagesCollection>();
    GetIt.I.unregister<ChatsCollection>();
  });

  test('create message success', () async {
    when(messagesCollection.createMessage(message1))
        .thenAnswer((_) => Future.value(message1));
    final message = await messagesResource.create(message1.json);
    expect(message, message1);
  });

  test('create message failed', () async {
    when(messagesCollection.createMessage(any))
        .thenAnswer((_) => Future.value(null));
    expect(() {
      return messagesResource.create(null);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('find message success', () async {
    when(messagesCollection.find(argThat(predicate(
            (x) =>
                (x.map['\$query']['author']) ==
                ObjectId.fromHexString(context1['subject']),
            'is even'))))
        .thenAnswer((_) => Stream.fromIterable([message4]));
    final messages = await messagesResource.find(context1);
    expect(messages, [message4]);
  });

  test('find message by id success', () async {
    when(messagesCollection.findOne(messageId4))
        .thenAnswer((_) => Future.value(message4));
    final message =
        await messagesResource.findMessage(messageId4.value, context1);

    expect(message, message4);
  });

  test('message with this id not found', () async {
    when(messagesCollection.findOne(any)).thenAnswer((_) => Future.value(null));
    expect(() {
      return messagesResource.findMessage(messageId4.value, context1);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('update message success', () async {
    when(messagesCollection.updateMessage(updatedMessage))
        .thenAnswer((_) => Future.value(updatedMessage));
    when(messagesCollection.findOne(messageId4))
        .thenAnswer((_) => Future.value(message4));
    final message = await messagesResource.editMessage(
        messageId4.value, updatedMessage.json, context1);
    expect(message, updatedMessage);
  });

  test('updated message or request body not found', () {
    when(messagesCollection.updateMessage(any))
        .thenAnswer((_) => Future.value(null));
    when(messagesCollection.findOne(any)).thenAnswer((_) => Future.value(null));
    expect(() {
      return messagesResource.editMessage(null, null, context1);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('delete message success', () async {
    when(messagesCollection.findOne(messageId4))
        .thenAnswer((_) => Future.value(message4));
    when(chatsCollection.findOne(message4.chatId))
        .thenAnswer((_) => Future.value(chat4));
    when(messagesCollection.delete(messageId4))
        .thenAnswer((_) => Future.value(message4));
    when(messagesCollection.find(argThat(predicate(
            (x) =>
                (x.map['\$query']['chatId']) ==
                ObjectId.fromHexString(message4.chatId.value),
            'is even'))))
        .thenAnswer((_) => Stream.fromIterable([message3]));
    final messages =
        await messagesResource.deleteMessage(messageId4.value, context1);
    expect(messages, [message3]);
  });

  test('deleted message not found', () async {
    when(messagesCollection.findOne(any)).thenAnswer((_) => Future.value(null));
    when(chatsCollection.findOne(message4.chatId))
        .thenAnswer((_) => Future.value(chat4));
    when(messagesCollection.delete(messageId4))
        .thenAnswer((_) => Future.value(message4));
    when(messagesCollection.find(argThat(predicate(
            (x) =>
                (x.map['\$query']['chatId']) ==
                ObjectId.fromHexString(message4.chatId.value),
            'is even'))))
        .thenAnswer((_) => Stream.fromIterable([null]));
    expect(() {
      return messagesResource.deleteMessage(messageId4.value, context1);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('deleted message by not found chat', () async {
    when(messagesCollection.findOne(message4.id))
        .thenAnswer((_) => Future.value(message4));
    when(chatsCollection.findOne(message4.chatId))
        .thenAnswer((_) => Future.value(null));
    when(messagesCollection.delete(messageId4))
        .thenAnswer((_) => Future.value(message4));
    when(messagesCollection.find(argThat(predicate(
            (x) =>
                (x.map['\$query']['chatId']) ==
                ObjectId.fromHexString(message4.chatId.value),
            'is even'))))
        .thenAnswer((_) => Stream.fromIterable([null]));
    expect(() {
      return messagesResource.deleteMessage(messageId4.value, context1);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });
}

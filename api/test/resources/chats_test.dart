import 'package:api/collections.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import 'package:api/src/resources/chats.dart';
import 'package:api/src/resources/messages.dart';
import 'package:api/src/resources/users.dart';
import 'package:models/models.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'package:rest_api_server/src/service_registry.dart';
import 'package:get_it/get_it.dart';
import 'testModels.dart';

class MessagesCollectionMock extends Mock implements MessagesCollection {}

class ChatsCollectionMock extends Mock implements ChatsCollection {}

class UsersCollectionMock extends Mock implements UsersCollection {}

void main() {
  MessagesCollection messagesCollection;
  Chats chatsResource;
  ChatsCollection chatsCollection;
  UsersCollection usersCollection;
  setUp(() {
    messagesCollection = MessagesCollectionMock();
    chatsCollection = ChatsCollectionMock();
    usersCollection = UsersCollectionMock();
    register<MessagesCollection>(messagesCollection);
    register<ChatsCollection>(chatsCollection);
    register<UsersCollection>(usersCollection);
    chatsResource = Chats();
  });
  tearDown(() {
    GetIt.I.unregister<MessagesCollection>();
    GetIt.I.unregister<ChatsCollection>();
    GetIt.I.unregister<UsersCollection>();
  });

  test('create chat success', () async {
    when(chatsCollection.createChat(chat4))
        .thenAnswer((_) => Future.value(chat4));
    final chat = await chatsResource.create(chat4.json, context1);
    expect(chat, chat4);
  });

  test('create chat failed', () {
    when(chatsCollection.createChat(any)).thenAnswer((_) => Future.value(null));
    expect(() {
      return chatsResource.create(null, context1);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('find chat success', () async {
    when(chatsCollection.findOne(chatId4))
        .thenAnswer((_) => Future.value(chat4));
    final chat = await chatsResource.findChat(chatId4.value, context1);
    expect(chat, chat4);
  });

  test('find chat not found', () async {
    when(chatsCollection.findOne(any)).thenAnswer((_) => Future.value(null));
    expect(() {
      return chatsResource.findChat('id', context1);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('update chat success', () async {
    when(chatsCollection.updateChat(chat4))
        .thenAnswer((_) => Future.value(chat4));
    when(chatsCollection.findOne(chatId4))
        .thenAnswer((_) => Future.value(chat4));
    final chat =
        await chatsResource.update(chatId4.value, chat4.json, context1);
    expect(chat, chat4);
  });

  test('updated chat or request body not found', () {
    when(chatsCollection.updateChat(any)).thenAnswer((_) => Future.value(null));
    when(chatsCollection.findOne(any)).thenAnswer((_) => Future.value(null));
    expect(() {
      return chatsResource.update(null, null, context1);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('add member success', () async {
    when(chatsCollection.findOne(chatId4))
        .thenAnswer((_) => Future.value(chat4));
    when(usersCollection.findOne(user5.id))
        .thenAnswer((_) => Future.value(user5));
    when(chatsCollection.updateChat(newChat))
        .thenAnswer((_) => Future.value(newChat));
    final members =
        await chatsResource.addMember(chatId4.value, user5.json, context1);
    expect(members, newChat.members);
  });

  test('being added member not found', () async {
    when(chatsCollection.findOne(chatId4))
        .thenAnswer((_) => Future.value(chat4));
    when(usersCollection.findOne(user5.id))
        .thenAnswer((_) => Future.value(null));
    when(chatsCollection.updateChat(newChat))
        .thenAnswer((_) => Future.value(newChat));
    expect(() {
      return chatsResource.addMember(chatId4.value, user5.json, context1);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('the chat to which the member is added was not found', () async {
    when(chatsCollection.findOne(chatId4))
        .thenAnswer((_) => Future.value(null));
    when(usersCollection.findOne(user5.id))
        .thenAnswer((_) => Future.value(user5));
    when(chatsCollection.updateChat(newChat))
        .thenAnswer((_) => Future.value(newChat));
    expect(() {
      return chatsResource.addMember(chatId4.value, user5.json, context1);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('delete member from chat success', () async {
    when(chatsCollection.findOne(chatId4))
        .thenAnswer((_) => Future.value(chat4));
    when(usersCollection.findOne(userId4))
        .thenAnswer((_) => Future.value(user4));
    when(chatsCollection.updateChat(Chat.fromJson(chat4.json
          ..['members'].removeWhere((v) => v['id'] == userId4.value))))
        .thenAnswer((_) => Future.value(Chat.fromJson(chat4.json
          ..['members'].removeWhere((v) => v['id'] == userId4.value))));
    final members = await chatsResource.deleteMember(
        chatId4.value, userId4.value, context1);
    expect(
        members,
        (Chat.fromJson(chat4.json
              ..['members'].removeWhere((v) => v['id'] == userId4.value)))
            .json['members']);
  });

  test('being deleted from chat member not found', () async {
    when(chatsCollection.findOne(chatId4))
        .thenAnswer((_) => Future.value(chat4));
    when(usersCollection.findOne(any)).thenAnswer((_) => Future.value(null));
    when(chatsCollection.updateChat(Chat.fromJson(chat4.json
          ..['members'].removeWhere((v) => v['id'] == userId4.value))))
        .thenAnswer((_) => Future.value(Chat.fromJson(chat4.json
          ..['members'].removeWhere((v) => v['id'] == userId4.value))));
    expect(() {
      return chatsResource.deleteMember(chatId4.value, userId4.value, context1);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('chat with being deleted member not found', () async {
    when(chatsCollection.findOne(any)).thenAnswer((_) => Future.value(null));
    when(usersCollection.findOne(userId4))
        .thenAnswer((_) => Future.value(user4));
    when(chatsCollection.updateChat(Chat.fromJson(any)))
        .thenAnswer((_) => Future.value(null));
    expect(() {
      return chatsResource.deleteMember(chatId4.value, userId4.value, context1);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });
}

import 'package:api/collections.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import 'package:api/src/resources/users.dart';
import 'package:rest_api_server/src/service_registry.dart';
import 'package:get_it/get_it.dart';
import 'package:models/models.dart';
import 'testModels.dart';
import 'package:mongo_dart/mongo_dart.dart';

class UsersCollectionMock extends Mock implements UsersCollection {}

class FriendsCollectionMock extends Mock implements FriendsCollection {}

void main() {
  UsersCollection usersCollection;
  FriendsCollection friendsCollection;
  Users usersResource;
  setUp(() {
    usersCollection = UsersCollectionMock();
    friendsCollection = FriendsCollectionMock();
    register<UsersCollection>(usersCollection);
    register<FriendsCollection>(friendsCollection);
    usersResource = Users();
  });
  tearDown(() {
    GetIt.I.unregister<UsersCollection>();
    GetIt.I.unregister<FriendsCollection>();
  });

  test('get user sucess', () async {
    when(usersCollection.findOne(UserId('1')))
        .thenAnswer((_) => Future.value(user1));
    final foundUser = await usersResource.findUser('1');
    expect(foundUser, user1);
  });
  test('user not found', () async {
    when(usersCollection.findOne(any)).thenAnswer((_) => Future.value(null));
    expect(() {
      return usersResource.findUser('id');
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('create user', () async {
    when(usersCollection.insert(user1)).thenAnswer((_) => Future.value(user1));
    final createUser = await usersResource.create(user1.json);
    expect(createUser, user1);
  });

  test('create user failed', () {
    when(usersCollection.insert(any)).thenAnswer((_) => Future.value(null));
    expect(() {
      return usersResource.create(null);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('add friend sucess', () async {
    when(usersCollection.findOne(userId1))
        .thenAnswer((_) => Future.value(user1));
    when(usersCollection.findOne(userId2))
        .thenAnswer((_) => Future.value(user2));
    when(friendsCollection.addFriend(userId1, userId2))
        .thenAnswer((_) => Stream.fromIterable([user2]));
    final friends =
        await usersResource.addFriend(userId1.value, user2.json, context);
    expect(friends, [user2]);
  });

  test('being added friend or user is not available', () async {
    when(usersCollection.findOne(any)).thenAnswer((_) => Future.value(null));
    expect(() {
      return usersResource.addFriend(userId1.value, user2.json, context);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('delete friend sucess', () async {
    when(usersCollection.findOne(userId1))
        .thenAnswer((_) => Future.value(user1));
    when(usersCollection.findOne(userId2))
        .thenAnswer((_) => Future.value(user2));
    when(friendsCollection.deleteFriend(userId1, userId2))
        .thenAnswer((_) => Stream.fromIterable([user3]));
    final friends =
        await usersResource.deleteFriend(userId1.value, user2.json, context);
    expect(friends, [user3]);
  });
  test('deleted friend or user is not avaliable', () async {
    when(usersCollection.findOne(any)).thenAnswer((_) => Future.value(null));

    expect(() {
      return usersResource.deleteFriend(userId1.value, user2.json, context);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('update user sucess', () async {
    when(usersCollection.findOne(userId1))
        .thenAnswer((_) => Future.value(user1));
    when(usersCollection.update(user1Updated))
        .thenAnswer((_) => Future.value(user1Updated));
    final updateUser =
        await usersResource.update(userId1.value, user1Updated.json, context);
    expect(updateUser, user1Updated);
  });

  test('updated user is not available', () {
    when(usersCollection.findOne(any)).thenAnswer((_) => Future.value(null));
    expect(() {
      return usersResource.update(userId1.value, user1Updated.json, context);
    }, throwsA(TypeMatcher<NotFoundException>()));
  });

  test('block user sucess', () async {
    when(usersCollection.findOne(userId4))
        .thenAnswer((_) => Future.value(user4));
    when(usersCollection
            .update(User.fromJson(user4.json..addAll({'deleted': true}))))
        .thenAnswer((_) =>
            Future.value(User.fromJson(user4.json..addAll({'deleted': true}))));
    final blockedUser =
        await usersResource.blockUser('5f7cd512c6336ac3f859b8d1');
    expect(blockedUser, User.fromJson(user4.json..addAll({'deleted': true})));
  });

  test('blocked user not found', () async {
    when(usersCollection.findOne(any)).thenAnswer((_) => Future.value(null));

    expect(() {
      return usersResource.blockUser('id');
    }, throwsA(TypeMatcher<NotFoundException>()));
  });
}

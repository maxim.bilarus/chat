import 'dart:io';
import 'dart:convert';

import 'package:api/collections.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import 'package:api/src/resources/login.dart';
import 'package:rest_api_server/src/service_registry.dart';
import 'package:get_it/get_it.dart';
import 'testModels.dart';

class UsersCollectionMock extends Mock implements UsersCollection {}

void main() {
  UsersCollection usersCollection;
  Login loginResource;
  setUp(() {
    usersCollection = UsersCollectionMock();
    register<UsersCollection>(usersCollection);
    loginResource = Login();
  });
  tearDown(() {
    GetIt.I.unregister<UsersCollection>();
  });
  test('login failed', () {
    when(usersCollection.find(any)).thenAnswer((_) => Stream.fromIterable([]));
    expect(() {
      return loginResource.login({
        'login': 'login1',
        'password': 'password1',
      });
    }, throwsA(TypeMatcher<UnauthorizedException>()));
  });

  test('login sucess', () async {
    when(usersCollection.find(argThat(predicate<SelectorBuilder>((q) {
      return q.map['\$query']['\$and'][0]['login'] == 'login1' &&
          q.map['\$query']['\$and'][1]['password'] == 'password1';
    })))).thenAnswer((_) => Stream.fromIterable([user1]));
    final response = await loginResource.login({
      'id': '1',
      'login': 'login1',
      'password': 'password1',
    });
    expect(response.statusCode, equals(HttpStatus.ok));
    final body = await response.readAsString();
    final userJson = json.decode(body);
    expect(userJson, equals(user1.json..remove('password')));
  });
  test('context and headers', () async {
    when(usersCollection.find(argThat(predicate<SelectorBuilder>((q) {
      return q.map['\$query']['\$and'][0]['login'] == 'login1' &&
          q.map['\$query']['\$and'][1]['password'] == 'password1';
    })))).thenAnswer((_) => Stream.fromIterable([user1]));
    final response = await loginResource.login({
      'id': '1',
      'login': 'login1',
      'password': 'password1',
    });

    expect(response.context, {'subject': user1.id.json, 'payload': user1.json});
    expect(response.headers,
        containsPair('Content-Type', ContentType.json.toString()));
  });
}

import 'package:models/models.dart';

final context = {'subject': user1.id.value, 'payload': user1.json};
final context1 = {'subject': user4.id.value, 'payload': user4.json};
final userId4 = UserId('5f7cd512c6336ac3f859b8d1');
final userId5 = UserId('5f7cd512c6336ac3f859b8d2');
final userId1 = UserId('1');
final userId2 = UserId('2');
final userId3 = UserId('3');
final user5 = User(id: userId5, login: 'login5', password: 'password5');
final user4 = User(id: userId4, login: 'login4', password: 'password4');
final user1 = User(id: userId1, login: 'login1', password: 'password1');
final user2 = User(id: userId2, login: 'login2', password: 'password2');
final user3 = User(id: userId3, login: 'login3', password: 'password3');
final user1Updated =
    User(id: userId1, login: 'login1Updated', password: 'password1Updated');
final userBoris = User(
    id: userId5,
    login: 'login5',
    password: 'password5',
    profile: Profile(
      name: 'Boris',
      surname: 'Petrov',
      email: 'boris@mail.ru',
    ));
final messageId4 = MessageId('5f7cd512c6336ac3f859b8d1');
final messageId1 = MessageId('1');
final messageId2 = MessageId('2');
final messageId3 = MessageId('3');
final message4 = Message(
    id: messageId4,
    text: 'text4',
    author: user4,
    chatId: chatId4,
    datetime: DateTime.now());
final message1 = Message(
    id: messageId1,
    text: 'text1',
    author: user1,
    chatId: chatId1,
    datetime: DateTime.now());
final message2 = Message(
    id: messageId2,
    text: 'text2',
    author: user2,
    chatId: chatId2,
    datetime: DateTime.now());
final message3 = Message(
    id: messageId3,
    text: 'text3',
    author: user3,
    chatId: chatId3,
    datetime: DateTime.now());
final updatedMessage = Message(
  id: messageId4,
  text: 'text123',
  author: user4,
  chatId: chatId4,
  datetime: DateTime.now(),
);
final chatId4 = ChatId('5f7cd512c6336ac3f859b8d1');
final chatId1 = ChatId('1');
final chatId2 = ChatId('2');
final chatId3 = ChatId('3');
final chat4 = Chat(
  id: chatId4,
  title: 'title',
  creator: user4,
  members: [user4],
);
final chat1 = Chat(
  id: chatId1,
  title: 'title1',
  creator: user1,
  members: [user1, user2, user3],
);
final chat2 = Chat(
  id: chatId2,
  title: 'title2',
  creator: user2,
  members: [user1, user2, user3],
);
final chat3 = Chat(
  id: chatId3,
  title: 'title3',
  creator: user3,
  members: [user1, user2, user3],
);
final newChat = Chat(
  id: chatId4,
  title: 'title',
  creator: user4,
  members: [user4, user5],
);

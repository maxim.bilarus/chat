import 'package:rest_api_server/src/service_registry.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:api/collections.dart';
import 'package:get_it/get_it.dart';
import 'package:test/test.dart';
import 'package:models/models.dart';
import 'package:nanoid/nanoid.dart';

void main() {
  String dbName;
  Db mongoDb;
  setUp(() async {
    dbName = nanoid();
    mongoDb = Db('mongodb://127.0.0.1/$dbName');
    await mongoDb.open();
    register<MessagesCollection>(
        MessagesCollection(mongoDb.collection('messages')));
    register<UsersCollection>(UsersCollection(mongoDb.collection('users')));
  });
  tearDown(() {
    mongoDb.drop();
    GetIt.I.unregister<MessagesCollection>();
    GetIt.I.unregister<UsersCollection>();
  });
  final profileJson = {
    'name': 'name',
    'surname': 'surname',
    'nickname': 'nickname',
    'email': 'email',
  };
  final userJson1 = {
    'id': '5f7cd512c6336ac3f859c4f3',
    'login': 'login',
    'password': 'password',
    'profile': profileJson,
  };
  final mess = Message.fromJson({
    'id': '5f7cd512c6336ac3f859b8d1',
    'chatId': '5f6dc67eac7b310a87a20431',
    'author': userJson1,
    'text': '12345'
  });
  test('create message', () async {
    await mongoDb.collection('users').insert(userJson1
      ..addAll({'_id': ObjectId.fromHexString(userJson1['id'])})
      ..remove('id'));
    final message = await MessagesCollection(mongoDb.collection('messages'))
        .createMessage(mess);
    expect(
        mess.json
          ..remove('datetime')
          ..addAll({'id': message.json['id']}),
        message.json..remove('datetime'));
  });
  test('find message', () async {
    final chatId1 = ObjectId.fromHexString('5f6dc67eac7b310a87a20431');
    final authorId1 = ObjectId.fromHexString('5f7cd512c6336ac3f859c4f3');
    final messageId1 = ObjectId.fromHexString('5f7cd512c6336ac3f859b8d1');
    await mongoDb.collection('users').insert(userJson1
      ..addAll({'_id': authorId1})
      ..remove('id'));
    await mongoDb.collection('messages').insert(mess.json
      ..addAll({'_id': messageId1})
      ..remove('id')
      ..update('chatId', (chatId) => chatId1)
      ..update('author', (author) => authorId1));
    final message1 = await MessagesCollection(mongoDb.collection('messages'))
        .findOne(MessageId(messageId1.toHexString()));
    expect(
        message1.json..remove('datetime'),
        mess.json
          ..addAll({'id': messageId1.toHexString()})
          ..remove('datetime'));
  });

  test('message update success', () async {
    final chatId1 = ObjectId.fromHexString('5f6dc67eac7b310a87a20431');
    final authorId1 = ObjectId.fromHexString('5f7cd512c6336ac3f859c4f3');
    final messageId1 = ObjectId.fromHexString('5f7cd512c6336ac3f859b8d1');
    final userJson2 = {
      'id': '5f7cd512c6336ac3f859c4f3',
      'login': 'login',
      'password': 'password',
      'profile': profileJson,
    };

    await mongoDb.collection('users').insert(
        userJson2..addAll({'_id': ObjectId.fromHexString(userJson2['id'])}));
    await mongoDb.collection('messages').insert(mess.json
      ..addAll({'_id': messageId1})
      ..remove('id')
      ..update('chatId', (chatId) => chatId1)
      ..update('author', (author) => authorId1));
    final message = await MessagesCollection(mongoDb.collection('messages'))
        .updateMessage(Message.fromJson(
            mess.json..update('text', (text) => '12345678910')));
    expect(message.json, mess.json..update('text', (text) => '12345678910'));
  });
}

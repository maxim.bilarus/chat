import 'package:mockito/mockito.dart';
import 'package:rest_api_server/src/service_registry.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:api/collections.dart';
import 'package:get_it/get_it.dart';
import 'package:test/test.dart';
import 'package:models/models.dart';
import 'package:nanoid/nanoid.dart';

void main() async {
  String dbName;
  mongo.Db mongoDb;
  final profile = Profile(name: 'name', surname: 'surname', email: 'email');
  final profileJson = {'name': 'name', 'surname': 'surname', 'email': 'email'};
  final user = User(
      id: UserId(mongo.ObjectId().value.toHexString()),
      login: 'login',
      password: 'password',
      profile: profile);

  final userJson = {
    'id': mongo.ObjectId().value.toHexString(),
    'login': 'login',
    'password': 'password',
    'profile': profileJson
  };

  setUp(() async {
    dbName = nanoid();
    mongoDb = mongo.Db('mongodb://127.0.0.1/$dbName');
    await mongoDb.open();
    register<UsersCollection>(UsersCollection(mongoDb.collection('users')));
  });
  tearDown(() {
    mongoDb.drop();
    GetIt.I.unregister<UsersCollection>();
  });

  test('create model sucess', () {
    final user1 =
        UsersCollection(mongoDb.collection('users')).createModel(userJson);
    expect(user1.json, userJson);
  });

  test('insert user sucess', () async {
    final user1 =
        await UsersCollection(mongoDb.collection('users')).insert(user);
    expect(User.fromJson(user.json..remove('id')),
        User.fromJson(user1.json..remove('id')));
  });
  test('find user success', () async {
    final user2 =
        await UsersCollection(mongoDb.collection('users')).insert(user);
    final user1 = await UsersCollection(mongoDb.collection('users'))
        .find(mongo.where.eq('profile.name', 'name'))
        .toList();

    expect(user1, [user2]);
  });
}

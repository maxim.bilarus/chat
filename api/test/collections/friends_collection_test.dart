import 'package:nanoid/nanoid.dart';
import 'package:rest_api_server/src/service_registry.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:api/collections.dart';
import 'package:get_it/get_it.dart';
import 'package:test/test.dart';
import 'package:models/models.dart';

void main() async {
  String dbName;
  mongo.Db mongoDb;
  setUp(() async {
    dbName = nanoid();
    mongoDb = mongo.Db('mongodb://127.0.0.1/$dbName');
    await mongoDb.open();
    register<FriendsCollection>(
        FriendsCollection(mongoDb.collection('friends')));
    register<UsersCollection>(UsersCollection(mongoDb.collection('users')));
  });
  tearDown(() {
    mongoDb.drop();
    GetIt.I.unregister<FriendsCollection>();
    GetIt.I.unregister<UsersCollection>();
  });

  test('add friend', () async {
    final id1 = mongo.ObjectId.fromHexString('5f78606f8de3a596816df546');
    final id2 = mongo.ObjectId.fromHexString('5f7cd512c6336ac3f859b8d1');
    await FriendsCollection(mongoDb.collection('friends'))
        .addFriend(UserId(id1.toHexString()), UserId(id2.toHexString()));
    final friendDocuments = await mongoDb
        .collection('friends')
        .find(mongo.where
            .eq('user', id1)
            .eq('friend', id2)
            .or(mongo.where.eq('user', id2).eq('friend', id1)))
        .map((d) => d..updateAll((k, v) => v.toHexString()))
        .toList();

    expect(friendDocuments.map((d) => d..remove('_id')).toList(), [
      {
        'user': '5f78606f8de3a596816df546',
        'friend': '5f7cd512c6336ac3f859b8d1',
      },
      {
        'user': '5f7cd512c6336ac3f859b8d1',
        'friend': '5f78606f8de3a596816df546',
      }
    ]);
  });

  test('find friend', () async {
    final id1 = mongo.ObjectId.fromHexString('5f78606f8de3a596816df546');
    final id2 = mongo.ObjectId.fromHexString('5f7cd512c6336ac3f859b8d1');
    final userJsonList = [
      {
        '_id': id1,
        'login': 'u1_login',
        'password': 'u1_password',
        'profile': {
          'email': 'u1@gmail.com',
          'name': 'u1_name',
          'surname': 'u1_surname',
          'nickname': 'u1_nickname',
        }
      },
      {
        '_id': id2,
        'login': 'u2_login',
        'password': 'u2_password',
        'profile': {
          'email': 'u2@gmail.com',
          'name': 'u2_name',
          'surname': 'u2_surname',
          'nickname': 'u2_nickname',
        }
      }
    ];
    await mongoDb.collection('users').insertAll(userJsonList);
    final users = await mongoDb
        .collection('users')
        .find(mongo.where
            .eq('login', 'u1_login')
            .or(mongo.where.eq('login', 'u2_login')))
        .map((d) => d..update('_id', (v) => v.toHexString()))
        .toList();
    await mongoDb.collection('friends').insertAll([
      {
        'user': id1,
        'friend': id2,
      },
      {
        'user': id2,
        'friend': id1,
      }
    ]);
    final friends = await FriendsCollection(mongoDb.collection('friends'))
        .findFriends(UserId(users[0]['_id']))
        .toList();
    expect(
        User.fromJson(users[1]
          ..addAll({'id': users[1]['_id']})
          ..remove('_id')),
        User.fromJson(friends.first.json));
  });

  test('find friends of friend', () async {
    final id1 = mongo.ObjectId.fromHexString('5f78606f8de3a596816df546');
    final id2 = mongo.ObjectId.fromHexString('5f7cd512c6336ac3f859b8d1');
    final id3 = mongo.ObjectId.fromHexString('5f7cd512c6336ac3f859b8d3');

    final userJsonList = [
      {
        '_id': id1,
        'login': 'u1_login',
        'password': 'u1_password',
        'profile': {
          'email': 'u1@gmail.com',
          'name': 'u1_name',
          'surname': 'u1_surname',
        }
      },
      {
        '_id': id2,
        'login': 'u2_login',
        'password': 'u2_password',
        'profile': {
          'email': 'u2@gmail.com',
          'name': 'u2_name',
          'surname': 'u2_surname',
        }
      },
      {
        '_id': id3,
        'login': 'u3_login',
        'password': 'u3_password',
        'profile': {
          'email': 'u3@gmail.com',
          'name': 'u3_name',
          'surname': 'u3_surname',
        }
      }
    ];
    await mongoDb.collection('users').insertAll(userJsonList);
    await mongoDb.collection('friends').insertAll([
      {
        'user': id1,
        'friend': id2,
      },
      {
        'user': id2,
        'friend': id1,
      },
      {
        'user': id2,
        'friend': id3,
      },
      {
        'user': id3,
        'friend': id2,
      }
    ]);
    final users = await mongoDb
        .collection('users')
        .find(mongo.where.eq('login', 'u1_login').or(mongo.where
            .eq('login', 'u2_login')
            .or(mongo.where.eq('login', 'u3_login'))))
        .map((d) => d..update('_id', (v) => v.toHexString()))
        .toList();
    final friendsOfFriend =
        await FriendsCollection(mongoDb.collection('friends'))
            .findFriendsOfFriends(UserId(users[0]['_id']))
            .toList();
    print(friendsOfFriend.map((u) => u.json));
    expect(
        User.fromJson(users[2]
          ..addAll({'id': users[2]['_id']})
          ..remove('_id')),
        User.fromJson(friendsOfFriend.first.json));
  });
  test('delete friend', () async {
    final id1 = mongo.ObjectId.fromHexString('5f78606f8de3a596816df546');
    final id2 = mongo.ObjectId.fromHexString('5f7cd512c6336ac3f859b8d1');
    final id3 = mongo.ObjectId.fromHexString('5f78606f8de3a596816df545');
    final id4 = mongo.ObjectId.fromHexString('5f7cd512c6336ac3f859b8d2');
    final friendsList = [
      {'user': id1, 'friend': id2},
      {'user': id2, 'friend': id1},
      {'user': id1, 'friend': id3},
      {'user': id3, 'friend': id1},
      {'user': id4, 'friend': id3},
      {'user': id3, 'friend': id4},
    ];
    final userJsonList = <Map<String, dynamic>>[
      {
        '_id': id1,
        'login': 'u1_login',
        'password': 'u1_password',
        'profile': {
          'email': 'u1@gmail.com',
          'name': 'u1_name',
          'surname': 'u1_surname',
        }
      },
      {
        '_id': id2,
        'login': 'u2_login',
        'password': 'u2_password',
        'profile': {
          'email': 'u2@gmail.com',
          'name': 'u2_name',
          'surname': 'u2_surname',
        }
      },
      {
        '_id': id3,
        'login': 'u3_login',
        'password': 'u3_password',
        'profile': {
          'email': 'u3@gmail.com',
          'name': 'u3_name',
          'surname': 'u3_surname',
        }
      },
      {
        '_id': id4,
        'login': 'u4_login',
        'password': 'u4_password',
        'profile': {
          'email': 'u4@gmail.com',
          'name': 'u4_name',
          'surname': 'u4_surname',
        }
      }
    ];

    await mongoDb.collection('friends').insertAll(friendsList);
    await mongoDb.collection('users').insertAll(userJsonList);
    final friends = await FriendsCollection(mongoDb.collection('friends'))
        .deleteFriend(UserId(id1.toHexString()), UserId(id2.toHexString()))
        .toList();
    expect([
      userJsonList[2]
        ..addAll({'id': userJsonList[2]['_id'].toHexString()})
        ..remove('_id')
    ], [
      friends.first.json
    ]);
  });
}

import 'package:nanoid/nanoid.dart';
import 'package:rest_api_server/src/service_registry.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:api/collections.dart';
import 'package:get_it/get_it.dart';
import 'package:test/test.dart';
import 'package:models/models.dart';

void main() async {
  String dbName;
  mongo.Db mongoDb;
  setUp(() async {
    dbName = nanoid();
    mongoDb = mongo.Db('mongodb://127.0.0.1/$dbName');
    await mongoDb.open();
    register<ChatsCollection>(ChatsCollection(mongoDb.collection('chats')));
    register<UsersCollection>(UsersCollection(mongoDb.collection('users')));
  });
  tearDown(() {
    mongoDb.drop();
    GetIt.I.unregister<ChatsCollection>();
    GetIt.I.unregister<UsersCollection>();
  });

  test('create chat', () async {
    final userJson = <String, dynamic>{
      'login': 'login',
      'password': 'password',
      'profile': {
        'email': 'email',
        'name': 'name',
        'surname': 'surname',
        'nickname': 'nickname',
      }
    }..addAll({'_id': mongo.ObjectId()});
    await mongoDb.collection('users').insert(userJson);
    var chatJson = <String, dynamic>{
      'title': 'title',
      'members': [
        userJson..addAll({'id': userJson['_id'].value.toHexString()})
      ],
      'creator': userJson..addAll({'id': userJson['_id'].value.toHexString()}),
    };
    final chat = await ChatsCollection(mongoDb.collection('chats'))
        .createChat(Chat.fromJson(chatJson));
    expect(
        chatJson
          ..update(
              'members',
              (members) =>
                  (members as List).map((m) => m..remove('_id')).toList())
          ..update('creator', (creator) => creator..remove('_id'))
          ..addAll({'id': chat.json['id']})
          ..remove('datetime'),
        chat.json..remove('datetime'));
  });

  test('find chat', () async {
    final userId = mongo.ObjectId.fromHexString('5f78606f8de3a596816df546');
    final chatId = mongo.ObjectId.fromHexString('5f78606f8de3a596816df545');

    final userJson = <String, dynamic>{
      '_id': userId,
      'login': 'login',
      'password': 'password',
      'profile': {
        'name': 'name',
        'surname': 'surname',
        'email': 'email',
      }
    };
    final chatJson = <String, dynamic>{
      '_id': chatId,
      'title': 'title',
      'creator': userJson['_id'],
      'members': [userJson['_id']],
    };
    await mongoDb.collection('users').insert(userJson);
    await mongoDb.collection('chats').insert(chatJson);
    final chat = await ChatsCollection(mongoDb.collection('chats'))
        .findOne(ChatId(chatId.toHexString()));

    expect(
        chat.json..remove('datetime'),
        chatJson
          ..update(
              'creator',
              (creator) => userJson
                ..addAll({'id': userId.value.toHexString()})
                ..remove('_id'))
          ..update(
              'members',
              (members) => (members as List)
                  .map((m) => userJson
                    ..addAll({'id': userId.value.toHexString()})
                    ..remove('_id'))
                  .toList())
          ..remove('datetime')
          ..addAll({'id': chatJson['_id'].toHexString()})
          ..remove('_id'));
  });
  test('update chat success', () async {
    final userJson = {
      '_id': mongo.ObjectId.fromHexString('5f78606f8de3a596816df546'),
      'login': 'login',
      'password': 'password',
      'profile': {
        'name': 'name',
        'surname': 'surname',
        'email': 'email',
      }
    };
    final user = User(
        id: UserId('5f78606f8de3a596816df546'),
        login: 'login',
        password: 'password',
        profile: Profile(name: 'name', surname: 'surname', email: 'email'));

    final chatJson = {
      '_id': mongo.ObjectId.fromHexString('5f78606f8de3a596816df545'),
      'title': 'title',
      'creator': userJson['_id'],
      'members': [userJson['_id']],
    };
    final updatedChat = Chat.fromJson({
      'id': '5f78606f8de3a596816df545',
      'title': 'newTitle',
      'creator': user.json,
      'members': [user.json],
    });
    await mongoDb.collection('users').insert(userJson);
    await mongoDb.collection('chats').insert(chatJson);
    final updatedChat1 = await ChatsCollection(mongoDb.collection('chats'))
        .updateChat(updatedChat);
    expect(updatedChat1, updatedChat);
  });
}

import 'dart:io';
import 'package:nanoid/nanoid.dart';
import 'package:rest_api_server/src/service_registry.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:rest_api_server/api_server.dart';
import 'package:rest_api_server/http_exception_middleware.dart';
import 'package:rest_api_server/auth_middleware.dart';
import 'package:rest_api_server/cors_headers_middleware.dart';
import 'package:api/collections.dart';
// ignore: uri_does_not_exist
import 'package:api/routes.g.dart' as generated;

void main() async {
  final address = InternetAddress.anyIPv4;
  final port = 7777;
  final mongoDb = mongo.Db('mongodb://127.0.0.1/chat');
  await mongoDb.open();
  register<Jwt>(Jwt(
      securityKey: nanoid(),
      issuer: 'Some organization',
      maxAge: Duration(hours: 1)));
  register<UsersCollection>(UsersCollection(mongoDb.collection('users')));
  register<FriendsCollection>(FriendsCollection(mongoDb.collection('friends')));
  register<MessagesCollection>(
      MessagesCollection(mongoDb.collection('messages')));
  register<ChatsCollection>(ChatsCollection(mongoDb.collection('chats')));
  final router = Router(generated.routes);

  final apiServer = ApiServer(
      address: address,
      port: port,
      handler: shelf.Pipeline()
          .addMiddleware(HttpExceptionMiddleware())
          .addMiddleware(CorsHeadersMiddleware({
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Expose-Headers': 'Authorization, Content-Type',
            'Access-Control-Allow-Headers':
                'Authorization, Origin, X-Requested-With, Content-Type, Accept, Content-Disposition',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE'
          }))
          .addMiddleware(AuthMiddleware(loginPaths: {
            'POST': [
              '/login',
            ],
          }, exclude: const {
            'POST': [
              '/login',
              '/users',
            ]
          }, jwt: locateService<Jwt>()))
          .addHandler(router.handler));
  await apiServer.start();
  router.printRoutes();
}

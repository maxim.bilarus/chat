import 'package:rest_api_client/rest_api_client.dart';
import 'dart:html';
import 'package:universal_io/io.dart' as io;

//universalIO
ApiClient a = ApiClient(Uri.http('127.0.0.1:7777', '/'),
    onBeforeRequest: (request) => request.change(
        headers: Map.from(request.headers)
          ..addAll({
            'X-Requested-With': 'XMLHttpRequest',
            'Authorization': window.localStorage['jwt']
          })),
    onAfterResponse: (response) {
      saveToken(response.headers[io.HttpHeaders.authorizationHeader]);
      if (response.statusCode == HttpStatus.unauthorized) {
        window.localStorage['exceptionMessage'] = 'unauthorized';
      }
      return response;
    });

void saveToken(String token) {
  window.localStorage['jwt'] = token;
}

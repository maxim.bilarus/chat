import 'package:rest_api_client/rest_api_client.dart';
import 'package:models/models.dart';
import 'dart:async';
import 'package:data_model/data_model.dart';
import 'api_client.dart';

/// Клиент чатов
class Chats extends ResourceClient<Chat> {
  Chats(ApiClient apiClient) : super('/chats', apiClient);

  /// Создает модель чата из json
  @override
  Chat createModel(Map<String, dynamic> json) => Chat.fromJson(json);

  /// Создает чат
  @override
  Future<Chat> create(Chat chat,
      {Map<String, String> headers = const {}}) async {
    if (chat is! JsonEncodable) {
      throw ArgumentError.value(
          chat, 'chat', 'Creating object must be JsonEncodable');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.post,
        resourcePath: resourcePath,
        headers: headers,
        body: chat.json));
    return processResponse(response);
  }

  /// Ищет чаты
  @override
  Future read(dynamic obj, {Map<String, String> headers = const {}}) async {
    String path;
    Map<String, dynamic> queryParameters;
    if (obj is ChatId) {
      path = '$resourcePath/${obj.value}';
    } else if (obj is Map) {
      path = resourcePath;
      queryParameters = Map<String, dynamic>.from(obj);
    } else {
      throw ArgumentError.value(obj, 'obj', 'Read criteria must be an ChatId');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: path,
        headers: headers,
        queryParameters: queryParameters));
    return processResponse(response);
  }

  /// Ищем чаты по контексту
  Future<List<Chat>> getByContext(
      {Map<String, String> headers = const {}}) async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get, resourcePath: resourcePath, headers: headers));
    return processResponse(response);
  }

  /// Обновляет чат
  @override
  Future<Chat> update(Chat chat,
      {Map<String, String> headers = const {}}) async {
    if (!(chat is JsonEncodable)) {
      throw ArgumentError.value(
          chat, 'chat', 'Updating object must be JsonEncodable');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.patch,
        resourcePath: '$resourcePath/${chat.id.value}',
        headers: headers,
        body: chat.json));
    return processResponse(response);
  }

  /// Добавляет собеседника в чат
  Future<Chat> addMember(Chat chat, User user,
      {Map<String, String> headers = const {}}) async {
    if (user is! JsonEncodable) {
      throw ArgumentError.value(
          user, 'user', 'Being added object must be JsonEncodable');
    }
    if (chat is! JsonEncodable) {
      throw ArgumentError.value(chat, 'chat',
          'The object to which the user was to be added must be JsonEncodable');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.post,
        resourcePath: '$resourcePath/${chat.id.value}/members',
        headers: headers,
        body: user.json));
    return processResponse(response);
  }

  /// Удаляет собеседника из чата
  Future<Chat> deleteMember(Chat chat, User user,
      {Map<String, String> headers = const {}}) async {
    if (user is! JsonEncodable) {
      throw ArgumentError.value(
          user, 'user', 'Being deleted object must be JsonEncodable');
    }
    if (chat is! JsonEncodable) {
      throw ArgumentError.value(chat, 'chat',
          'The object to which the user was to be deleted must be JsonEncodable');
    }
    final response = await apiClient.send(ApiRequest(
      method: HttpMethod.delete,
      resourcePath: '$resourcePath/${chat.id.value}/members/${user.id.value}',
      headers: headers,
    ));
    return processResponse(response);
  }

  @override
  Future delete(dynamic _, {Map<String, String> headers = const {}}) async {
    throw UnsupportedError('method not appliable');
  }

  @override
  Future<Chat> replace(Chat _, {Map<String, String> headers = const {}}) async {
    throw UnsupportedError('method not appliable');
  }
}

final chats = Chats(a);

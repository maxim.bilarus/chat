import 'package:rest_api_client/rest_api_client.dart';
import 'package:models/models.dart';
import 'dart:async';
import 'package:data_model/data_model.dart';
import 'api_client.dart';

/// Клиент сообщений
class Messages extends ResourceClient<Message> {
  Messages(ApiClient apiClient) : super('/messages', apiClient);

  /// Создает модель сообщения из json
  @override
  Message createModel(Map<String, dynamic> json) => Message.fromJson(json);

  /// Создает сообщение
  @override
  Future<Message> create(Message message,
      {Map<String, String> headers = const {}}) async {
    if (message is! JsonEncodable) {
      throw ArgumentError.value(
          message, 'message', 'Creating object must be JsonEncodable');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.post,
        resourcePath: resourcePath,
        headers: headers,
        body: message.json));
    return processResponse(response);
  }

  /// Удаляет сообщение
  @override
  Future delete(dynamic message,
      {Map<String, String> headers = const {}}) async {
    String path;

    if (message is Message) {
      path =
          '$resourcePath/${message.chatId.value}/messages/${message.id.value}';
    } else {
      throw ArgumentError.value(
          message, 'message', 'Delete criteria must be an MessageId');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.delete, resourcePath: path, headers: headers));
    return processResponse(response);
  }

  /// Ищет сообщения
  @override
  Future read(dynamic obj, {Map<String, String> headers = const {}}) async {
    String path;
    Map<String, dynamic> queryParameters;
    if (obj is MessageId) {
      path = '$resourcePath/${obj.value}';
    } else if (obj is Map) {
      path = resourcePath;
      queryParameters = Map<String, dynamic>.from(obj);
    } else {
      throw ArgumentError.value(
          obj, 'obj', 'Read criteria must be an MessageId');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: path,
        headers: headers,
        queryParameters: queryParameters));
    return processResponse(response);
  }

  /// Обновляет сообщение
  @override
  Future<Message> update(Message message,
      {Map<String, String> headers = const {}}) async {
    if (!(message is JsonEncodable)) {
      throw ArgumentError.value(
          message, 'message', 'Updating object must be JsonEncodable');
    }
    final response = await apiClient.send(ApiRequest(
      method: HttpMethod.patch,
      resourcePath: '$resourcePath/${message.id.value}',
      headers: headers,
      body: message.json,
    ));
    return processResponse(response);
  }

  @override
  Future<Message> replace(Message _,
      {Map<String, String> headers = const {}}) async {
    throw UnsupportedError('method not appliable');
  }
}

final messages = Messages(a);

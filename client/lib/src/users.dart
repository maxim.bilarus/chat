import 'package:rest_api_client/rest_api_client.dart';
import 'package:models/models.dart';
import 'dart:async';
import 'dart:html';
import 'package:data_model/data_model.dart';
import 'api_client.dart';

/// Клиент пользователей
class Users extends ResourceClient<User> {
  Users(ApiClient apiClient) : super('/users', apiClient);

  /// Создает модель пользователя из json
  @override
  User createModel(Map<String, dynamic> json) => User.fromJson(json);

  /// Обновляет модель пользователя
  @override
  Future<User> update(User obj,
      {Map<String, String> headers = const {}}) async {
    if (!(obj is JsonEncodable)) {
      throw ArgumentError.value(
          obj, 'obj', 'Updating object must be JsonEncodable');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.patch,
        resourcePath: '$resourcePath/${obj.id.value}',
        headers: headers,
        body: obj.json));
    return processResponse(response);
  }

  /// Ищет пользователей
  @override
  Future read(dynamic obj, {Map<String, String> headers = const {}}) async {
    String path;
    Map<String, dynamic> queryParameters;

    if (obj is UserId) {
      path = '$resourcePath/${obj.value}';
    } else if (obj is Map) {
      path = resourcePath;
      queryParameters = obj;
    } else {
      throw ArgumentError.value(obj, 'obj',
          'Read criteria must be an UserId or String of query parameters');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: path,
        queryParameters: queryParameters,
        headers: headers));
    return processResponse(response);
  }

  /// Создает пользователя
  @override
  Future<User> create(User user,
      {Map<String, String> headers = const {}}) async {
    if (user is! JsonEncodable) {
      throw ArgumentError.value(
          user, 'user', 'Creating object must be JsonEncodable');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.post,
        resourcePath: resourcePath,
        headers: headers,
        body: user.json));
    return processResponse(response);
  }

  /// Позволяет пользователю авторизоваться
  Future<dynamic> login(String login, String password) async {
    ApiResponse response;
    dynamic error;

    try {
      response = await apiClient.send(ApiRequest(
          method: HttpMethod.post,
          resourcePath: '/login',
          body: {'login': login, 'password': password}));
    } catch (e) {
      error = e;
    }
    if (error != null) {
      return error;
    }
    if (response.statusCode != HttpStatus.ok) {
      return response.statusCode;
    }

    return processResponse(response);
  }

  /// Блокирует пользователя
  Future<User> blockUser(User user,
      {Map<String, String> headers = const {}}) async {
    if (user is! JsonEncodable) {
      throw ArgumentError.value(
          user, 'obj', 'Creating user must be JsonEncodable');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.patch,
        resourcePath: '$resourcePath/${user.id.value}/block',
        headers: headers));
    if (response.statusCode != HttpStatus.ok) {
      throw response.reasonPhrase;
    }
    return processResponse(response);
  }

  Future<List<User>> readFriends(
      UserId id, Map<String, dynamic> queryParameters,
      {Map<String, String> headers = const {}}) async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: '$resourcePath/${id.value}/friends',
        queryParameters: queryParameters,
        headers: headers));
    if (response.statusCode != HttpStatus.ok) {
      throw response.reasonPhrase;
    }
    return processResponse(response);
  }

  /// Добавляет друга
  Future<List<User>> addFriend(User user, User friend,
      {Map<String, String> headers = const {}}) async {
    if (user is! JsonEncodable) {
      throw ArgumentError.value(user, 'user',
          'The friend document that is being created has a user must be JsonEncodable');
    } else if (friend is! JsonEncodable) {
      throw ArgumentError.value(friend, 'friend',
          'The friend document that is being created has a friend must be JsonEncodable');
    }
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.post,
        resourcePath: '$resourcePath/${user.id.value}/friends',
        body: friend.json,
        headers: headers));
    if (response.statusCode != HttpStatus.ok) {
      throw response.reasonPhrase;
    }
    return processResponse(response);
  }

  /// Удаляет друга
  Future<List<User>> deleteFriend(User user, User friend,
      {Map<String, String> headers = const {}}) async {
    if (user is! JsonEncodable) {
      throw ArgumentError.value(user, 'user',
          'The friend document that is being deleted has a user must be JsonEncodable');
    }
    if (friend is! JsonEncodable) {
      throw ArgumentError.value(friend, 'friend',
          'The friend document that is being deleted has a friend must be JsonEncodable');
    }
    final response = await apiClient.send(ApiRequest(
      method: HttpMethod.delete,
      resourcePath: '$resourcePath/${user.id.value}/friends',
      body: friend.json,
      headers: headers,
    ));
    if (response.statusCode != HttpStatus.ok) {
      throw response.reasonPhrase;
    }
    return processResponse(response);
  }

  @override
  Future delete(dynamic _, {Map<String, String> headers = const {}}) {
    throw UnsupportedError('method not appliable');
  }

  @override
  Future<User> replace(User _, {Map<String, String> headers = const {}}) async {
    throw UnsupportedError('method not appliable');
  }
}

final users = Users(a);

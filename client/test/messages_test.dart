import 'package:rest_api_client/rest_api_client.dart';
import 'package:test/test.dart';
import 'package:client/client.dart';
import 'package:mockito/mockito.dart';
import 'testModels.dart';

class MockApiClient extends Mock implements ApiClient {}

void main() {
  Messages messages;
  ApiClient apiClient;
  setUp(() {
    apiClient = MockApiClient();
    messages = Messages(apiClient);
  });

  test('create message success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.post &&
        x.resourcePath == '/messages' &&
        x.body.toString() == newMessage.json.toString() &&
        x.headers.toString() ==
            const {}
                .toString()))))).thenAnswer((_) => Future<ApiResponse>.value(
        ApiResponse(body: message.json, statusCode: 200, headers: const {})));
    final message1 = await messages.create(newMessage);
    expect(message1.json..remove('id'), equals(newMessage.json));
  });

  test('being created object is not json encodable', () async {
    expect(() {
      return messages.create(null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });

  test('delete message success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method ==
            HttpMethod.delete &&
        x.resourcePath ==
            '/messages/${message.chatId.value}/messages/${message.id.value}' &&
        x.headers.toString() ==
            const {}.toString()))))).thenAnswer((_) =>
        Future<ApiResponse>.value(
            ApiResponse(body: [], statusCode: 200, headers: const {})));
    final messages1 = await messages.delete(message);
    expect(messages1, []);
  });

  test('being deleted object is not json encodable', () async {
    expect(() {
      return messages.delete(null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });

  test('read messages success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.get &&
        x.resourcePath == '/messages/${messageId.value}' &&
        x.headers.toString() ==
            const {}
                .toString()))))).thenAnswer((_) => Future<ApiResponse>.value(
        ApiResponse(body: message.json, statusCode: 200, headers: const {})));
    final messages1 = await messages.read(messageId);
    expect(messages1, equals(message));
  });

  test('the object passed to the read method is not a messageId', () {
    expect(() {
      return messages.read(null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });
  test('update message success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method ==
            HttpMethod.patch &&
        x.resourcePath == '/messages/${messageId.value}' &&
        x.body.toString() == updatedMessage.json.toString() &&
        x.headers.toString() ==
            const {}
                .toString()))))).thenAnswer((_) => Future<ApiResponse>.value(
        ApiResponse(body: updatedMessage, headers: const {}, statusCode: 200)));
    final updatedMessage1 = await messages.update(updatedMessage);
    expect(updatedMessage1, equals(updatedMessage));
  });

  test('being updated object is not json encodable', () async {
    expect(() {
      return messages.update(null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });
}

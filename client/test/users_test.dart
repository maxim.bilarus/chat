import 'package:rest_api_client/rest_api_client.dart';
import 'package:test/test.dart';
import 'package:client/client.dart';
import 'package:mockito/mockito.dart';
import 'testModels.dart';

class MockApiClient extends Mock implements ApiClient {}

void main() {
  Users users;
  ApiClient apiClient;
  setUp(() {
    apiClient = MockApiClient();
    users = Users(apiClient);
  });

  test('create user success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.post &&
            x.resourcePath == '/users' &&
            x.body.toString() == newUser.json.toString() &&
            x.headers.toString() == const {}.toString())))))
        .thenAnswer((_) => Future<ApiResponse>.value(
            ApiResponse(body: user.json, statusCode: 200, headers: const {})));
    final user1 = await users.create(newUser);
    expect(user1.json..remove('id'), equals(newUser.json));
  });

  test('object is not json encodable', () async {
    expect(() {
      return users.create(null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });

  test('read users success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.get &&
            x.resourcePath == '/users/${userId.value}' &&
            x.headers.toString() == const {}.toString())))))
        .thenAnswer((_) => Future<ApiResponse>.value(
            ApiResponse(body: user.json, statusCode: 200, headers: const {})));
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.get &&
            x.resourcePath == '/users' &&
            x.queryParameters.toString() == {'searchString': 'Ma'}.toString() &&
            x.headers.toString() == const {}.toString())))))
        .thenAnswer((_) => Future<ApiResponse>.value(
            ApiResponse(body: user.json, statusCode: 200, headers: const {})));
    final user1 = await users.read('Ma');
    final user2 = await users.read(userId);
    expect(user1.json, equals(user.json));
    expect(user2.json, equals(user2.json));
  });

  test('the object passed to the read method is not a user or a search string',
      () {
    expect(() {
      return users.read(null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });

  test('update user success', () async {
    when(apiClient.send(argThat(predicate((x) =>
        (x.method == HttpMethod.patch &&
            x.resourcePath == '/users/${updatedUser.id.value}' &&
            x.body.toString() == updatedUser.json.toString() &&
            x.headers.toString() == const {}.toString()))))).thenAnswer((_) =>
        Future<ApiResponse>.value(ApiResponse(
            body: updatedUser.json, statusCode: 200, headers: const {})));
    final updatedUser1 = await users.update(updatedUser);
    expect(updatedUser1, equals(updatedUser));
  });

  test('the object passed to the update method is json encodable', () {
    expect(() {
      return users.update(null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });

  test('login user success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.post &&
            x.body.toString() ==
                {'login': user.login, 'password': user.password}.toString() &&
            x.resourcePath == '/login')))))
        .thenAnswer((_) => Future<ApiResponse>.value(
            ApiResponse(body: user.json, statusCode: 200, headers: const {})));
    final user1 = await users.login(user.login, user.password);
    expect(user1, equals(user));
  });
  test('block user success', () async {
    when(apiClient.send(argThat(predicate((x) =>
        (x.method == HttpMethod.patch &&
            x.resourcePath == '/users/${user.id.value}/block' &&
            x.headers.toString() == const {}.toString()))))).thenAnswer((_) =>
        Future<ApiResponse>.value(ApiResponse(
            statusCode: 200,
            headers: const {},
            body: user.json..addAll({'deleted': true}))));
    final user1 = await users.blockUser(user);
    expect(user1.json, equals(user.json..addAll({'deleted': true})));
  });

  test('the object passed to the blockUser method is not a user', () {
    expect(() {
      return users.blockUser(null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });

  test('add friend success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.post &&
        x.resourcePath == '/users/${user.id.value}/friends' &&
        x.body.toString() ==
            member.json
                .toString()))))).thenAnswer((_) => Future<ApiResponse>.value(
        ApiResponse(body: [member.json], headers: const {}, statusCode: 200)));
    final friends = await users.addFriend(user, member);
    expect(friends, [member]);
  });

  test('the objects passed to the addFriend method is not json encodable', () {
    expect(() {
      return users.addFriend(null, null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });

  test('delete friends success', () async {
    when(apiClient.send(argThat(predicate((x) =>
        (x.method == HttpMethod.delete &&
            x.resourcePath == '/users/${user.id.value}/friends' &&
            x.body.toString() == member.json.toString() &&
            x.headers.toString() == const {}.toString()))))).thenAnswer((_) =>
        Future<ApiResponse>.value(
            ApiResponse(body: [], headers: const {}, statusCode: 200)));
    final friends = await users.deleteFriend(user, member);
    expect(friends, []);
  });

  test('the objects passed to the deleteFriend method is not json encodable',
      () {
    expect(() {
      return users.deleteFriend(null, null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });
}

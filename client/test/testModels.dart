import 'package:models/models.dart';

final newUser = User(
    login: 'mbilarus',
    password: '123',
    profile: Profile(
        name: 'Maxim', surname: 'Bilarus', email: 'maxim.bilarus@gmail.com'));
final newChat =
    Chat(title: 'FirstChat', creator: user, members: [user, member]);
final newMessage = Message(author: user, text: 'text', chatId: chat.id);
final message = Message(
    id: MessageId('6015612f4c0f3ef54598ad2d'),
    author: user,
    text: 'text',
    chatId: chat.id);
final updatedMessage = Message(
    id: MessageId('6015612f4c0f3ef54598ad2d'),
    author: user,
    text: 'updatedteeext!',
    chatId: chat.id);
final user = User(
    id: UserId('6015612f4c0f3ef54598ad2c'),
    login: 'mbilarus',
    password: '123',
    profile: Profile(
        name: 'Maxim', surname: 'Bilarus', email: 'maxim.bilarus@gmail.com'));
final updatedUser = User(
    id: UserId('6015612f4c0f3ef54598ad2c'),
    login: 'mbilarusnewnickname',
    password: '123',
    profile: Profile(
        name: 'Maxim', surname: 'Bilarus', email: 'maxim.bilarus@gmail.com'));
final member = User(
    id: UserId('5fafebc47466b71f9c5f4e68'),
    login: 'vasiliy',
    password: '123',
    profile: Profile(
        name: 'Vasiliy', surname: 'Petrov', email: 'vasiliy24@gmail.com'));
final chat = Chat(
    id: ChatId('5fafcbc47466b71f9c5f4e68'),
    title: 'FirstChat',
    creator: user,
    members: [user, member]);
final updatedChat = Chat(
    id: ChatId('5fafcbc47466b71f9c5f4e68'),
    title: 'newUpdatedChat!!!!!',
    creator: user,
    members: [user, member]);
final userId = UserId('6015612f4c0f3ef54598ad2c');
final messageId = message.id;
final chatId = chat.id;

import 'package:rest_api_client/rest_api_client.dart';
import 'package:test/test.dart';
import 'package:client/client.dart';
import 'package:mockito/mockito.dart';
import 'testModels.dart';

class MockApiClient extends Mock implements ApiClient {}

void main() {
  Chats chats;
  ApiClient apiClient;
  setUp(() {
    apiClient = MockApiClient();
    chats = Chats(apiClient);
  });

  test('create successs', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.post &&
            x.resourcePath == '/chats' &&
            x.body.toString() == newChat.json.toString() &&
            x.headers.toString() == const {}.toString())))))
        .thenAnswer((_) => Future.value(
            ApiResponse(headers: const {}, statusCode: 200, body: chat.json)));

    final chat1 = await chats.create(newChat);
    expect(chat1.json..remove('id'), equals(newChat.json));
  });

  test('being created object is not json encodable', () async {
    expect(() {
      return chats.create(null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });

  test('read chats success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.get &&
            x.resourcePath == '/chats/${chatId.value}' &&
            x.headers.toString() == const {}.toString())))))
        .thenAnswer((_) => Future<ApiResponse>.value(
            ApiResponse(body: chat.json, statusCode: 200, headers: const {})));

    final chat1 = await chats.read(chatId);
    expect(chat1, equals(chat));
  });

  test('the object passed to the read method is not a chatId', () async {
    expect(() {
      return chats.read(null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });

  test('get by context success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.get &&
        x.resourcePath == '/chats' &&
        x.headers.toString() ==
            const {}
                .toString()))))).thenAnswer((_) => Future<ApiResponse>.value(
        ApiResponse(body: [chat.json], statusCode: 200, headers: const {})));
    final chats1 = await chats.getByContext();
    expect(chats1, [chat]);
  });
  test('add member success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.post &&
            x.body.toString() == member.json.toString() &&
            x.resourcePath == '/chats/${chatId.value}/members' &&
            x.headers.toString() == const {}.toString())))))
        .thenAnswer((_) => Future<ApiResponse>.value(
            ApiResponse(body: chat, statusCode: 200, headers: const {})));
    final chat1 = await chats.addMember(chat, member);
    expect(chat1, chat);
  });

  test('being added objects is not json encodable', () async {
    expect(() {
      return chats.addMember(null, null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });
  test('delete member success', () async {
    when(apiClient.send(argThat(predicate((x) => (x.method == HttpMethod.delete &&
            x.headers.toString() == const {}.toString() &&
            x.resourcePath == '/chats/${chatId.value}/members/${member.id.value}')))))
        .thenAnswer((_) => Future<ApiResponse>.value(ApiResponse(
            headers: const {},
            statusCode: 200,
            body: chat.json..update('members', (members) => members..removeWhere((m) => m['login'] == 'vasiliy')).toList())));
    final chat1 = await chats.deleteMember(chat, member);
    expect(
        chat1.json,
        equals(chat.json
          ..update(
              'members',
              (members) => members
                ..removeWhere((m) => m['login'] == 'vasiliy')).toList()));
  });

  test('being deleted objects is not json encodable', () async {
    expect(() {
      return chats.deleteMember(null, null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });

  test('update chat success', () async {
    when(apiClient.send(argThat(predicate((x) =>
        (x.method == HttpMethod.patch &&
            x.body.toString() == updatedChat.json.toString() &&
            x.headers.toString() == const {}.toString() &&
            x.resourcePath == '/chats/${updatedChat.id.value}'))))).thenAnswer(
        (_) => Future<ApiResponse>.value(ApiResponse(
            body: updatedChat.json, headers: const {}, statusCode: 200)));
    final updatedChat1 = await chats.update(updatedChat);
    expect(updatedChat1, equals(updatedChat));
  });

  test('being update object is not json encodable', () async {
    expect(() {
      return chats.update(null);
    }, throwsA(TypeMatcher<ArgumentError>()));
  });
}

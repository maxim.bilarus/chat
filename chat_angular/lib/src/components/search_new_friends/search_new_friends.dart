import 'dart:convert';

import 'package:Chat_angular/src/services/users_service/users_service.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:models/models.dart';
import 'dart:html';
import 'package:angular_components/material_chips/material_chip.dart';
import 'package:angular_components/material_chips/material_chips.dart';

@Component(
  selector: 'search-new-friends',
  templateUrl: 'search_new_friends.html',
  styleUrls: ['search_new_friends.css'],
  directives: [
    materialInputDirectives,
    MaterialButtonComponent,
    NgModel,
    MaterialIconComponent,
    NgIf,
    NgFor,
    MaterialPopupComponent,
    MaterialTooltipDirective,
    PopupSourceDirective,
    DeferredContentDirective,
    MaterialChipComponent,
    MaterialChipsComponent,
    displayNameRendererDirective,
  ],
  pipes: [commonPipes],
  providers: [popupBindings],
)
class SearchNewFriends implements OnInit {
  String searchString;
  String searchMessage;

  User user;
  final visible = List.filled(2, false);

  final List<RelativePosition> _popupPositions = [
    RelativePosition.InlineTopLeft
  ];

  List<RelativePosition> get popupPositions => _popupPositions;
  final _jsonDecoder = JsonDecoder();

  final UsersService _usersService;

  SearchNewFriends(this._usersService);
  List<Future<List<User>>> futureFriendsOfFriends;
  dynamic friendsOfFriends;
  List<User> friends;
  List<User> users = [];
  List<User> possibleFriends = [];
  String minYearsOld;
  String maxYearsOld;
  String patronymic;
  List<Chip> chips = <Chip>[];

  @override
  void ngOnInit() async {
    user = User.fromJson(_jsonDecoder.convert(window.localStorage['user']));
    friends = await _usersService.findFriends({}, user.id);
    print(friends);
    possibleFriends =
        await _usersService.findFriends({'friendsOfFriend': 'true'}, user.id);
    users = possibleFriends;
    if (users != null) {
      searchMessage = 'Possible friends:';
    } else {
      searchMessage = 'To find friends enter a query.';
    }
  }

  @ViewChild('minYearsOldInput')
  MaterialInputComponent minYearsOldInput;

  @ViewChild('maxYearsOldInput')
  MaterialInputComponent maxYearsOldInput;

  @ViewChild('PatronymucInput')
  MaterialInputComponent patronymicInput;

  void find() async {
    dynamic queryParams = <String, dynamic>{};
    if ((searchString?.trim()?.isEmpty == true ||
            searchString?.trim()?.isEmpty == null) &&
        (patronymic?.trim()?.isEmpty == true ||
            patronymic?.trim()?.isEmpty == null) &&
        (minYearsOld?.trim()?.isEmpty == true ||
            minYearsOld?.trim()?.isEmpty == null) &&
        (maxYearsOld?.trim()?.isEmpty == true ||
            maxYearsOld?.trim()?.isEmpty == null)) {
      users = possibleFriends;
      if (users.isNotEmpty) {
        searchMessage = 'Possible friends:';
      } else {
        searchMessage = 'To find friends enter a query.';
      }
    } else {
      if (maxYearsOld?.trim()?.isEmpty != null &&
          maxYearsOld?.trim()?.isEmpty != true) {
        queryParams.addAll({'yearsOldEnd': maxYearsOld});
      }
      if (minYearsOld?.trim()?.isEmpty != null &&
          minYearsOld?.trim()?.isEmpty != true) {
        queryParams.addAll({'yearsOldStart': minYearsOld});
      }
      if (patronymic?.trim()?.isEmpty != null &&
          patronymic?.trim()?.isEmpty != true) {
        queryParams.addAll({'patronymic': patronymic});
      }
      if (searchString?.trim()?.isEmpty != null &&
          searchString?.trim()?.isEmpty != true) {
        queryParams.addAll({'searchString': searchString});
      }
      users = ((await _usersService.find(queryParams))
        ..removeWhere((u) =>
            (u
              ..login = null
              ..password = null) ==
            user))
        ..map((u) {
          friends
            ..map((f) {
              if (f == u) {
                users.remove(u);
              }
            });
        });

      if (users.isNotEmpty) {
        searchMessage = 'By query finded ${users.length} users(s):';
      } else {
        searchMessage = 'No users could be found for this query.';
      }
    }
  }

  void saveSearchParams() async {
    chips = [];
    if (minYearsOld?.trim()?.isEmpty != true &&
        minYearsOld?.trim()?.isEmpty != null) {
      chips.add(Chip('minYearsOld: ${minYearsOld.toString()}'));
    }
    if (maxYearsOld?.trim()?.isEmpty != true &&
        maxYearsOld?.trim()?.isEmpty != null) {
      chips.add(Chip('maxYearsOld: ${maxYearsOld.toString()}'));
    }
    if (patronymic?.trim()?.isEmpty != true &&
        patronymic?.trim()?.isEmpty != null) {
      chips.add(Chip('patronymic: ${patronymic.toString()}'));
    }
  }

  void removeChipAndQueryParam(Chip chip) async {
    chips.remove(chip);
    if (chip.uiDisplayName == 'maxYearsOld: ${maxYearsOld.toString()}') {
      maxYearsOld = '';
    }
    if (chip.uiDisplayName == 'minYearsOld: ${minYearsOld.toString()}') {
      minYearsOld = '';
    }
    if (chip.uiDisplayName == 'patronymic: ${patronymic.toString()}') {
      patronymic = '';
    }
  }
}

class Chip implements HasUIDisplayName {
  @override
  final String uiDisplayName;
  const Chip(this.uiDisplayName);
}

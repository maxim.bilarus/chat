import 'package:Chat_angular/src/services/users_service/users_service.dart';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import 'package:Chat_angular/src/routes/routes.dart';

@Component(
  selector: 'my-app',
  styleUrls: ['app_component.css'],
  templateUrl: 'app_component.html',
  exports: [RoutePaths, Routes],
  directives: [routerDirectives],
  providers: [ClassProvider(UsersService)],
)
class AppComponent {}

import 'dart:convert';

import 'package:Chat_angular/src/services/chats_service/chats_service.dart';
import 'package:Chat_angular/src/services/messages_service/messages_service.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:models/models.dart';
import 'dart:html';

@Component(
    selector: 'search-in-chats',
    providers: [ClassProvider(ChatsService), ClassProvider(MessagesService)],
    templateUrl: 'search_in_chats.html',
    styleUrls: ['search_in_chats.css'],
    directives: [
      materialInputDirectives,
      MaterialButtonComponent,
      NgModel,
      MaterialIconComponent,
      NgIf,
      NgFor,
      MaterialPopupComponent,
      MaterialTooltipDirective,
      PopupSourceDirective,
      DeferredContentDirective,
      MaterialChipComponent,
      MaterialChipsComponent,
      displayNameRendererDirective,
    ])
class SearchInChats implements OnInit {
  final ChatsService _chatsService;
  final MessagesService _messagesService;
  SearchInChats(this._chatsService, this._messagesService);
  List<Chat> chats;
  List<Message> messages = [];
  User user;
  String searchString;
  String searchMessage;
  final JsonDecoder _jsonDecoder = JsonDecoder();
  List<Chip> chips;

  final visible = List.filled(2, false);

  final List<RelativePosition> _popupPositions = [
    RelativePosition.InlineTopLeft
  ];

  List<RelativePosition> get popupPositions => _popupPositions;
  @override
  void ngOnInit() async {
    user = User.fromJson(_jsonDecoder.convert(window.localStorage['user']));
    chats = await _chatsService.find({'memberId': user.id.value});
  }

  void saveSearchParams() async {
    chips = [];
  }

  void removeChipAndQueryParam(Chip chip) async {}

  void find() {}
}

class Chip implements HasUIDisplayName {
  @override
  final String uiDisplayName;
  const Chip(this.uiDisplayName);
}

import 'dart:async';
import 'dart:convert';

import 'package:Chat_angular/src/components/search_in_friends/search_in_friends.dart';
import 'package:Chat_angular/src/components/search_new_friends/search_new_friends.dart';
import 'package:Chat_angular/src/services/users_service/users_service.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/material_input/material_input.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angular_router/angular_router.dart';
import 'package:stream_transform/stream_transform.dart';
import 'package:models/models.dart';
import 'dart:html';

@Component(
    selector: 'add-friends-component',
    templateUrl: 'add_friends_component.html',
    styleUrls: [
      'add_friends_component.css'
    ],
    directives: [
      materialInputDirectives,
      MaterialButtonComponent,
      NgFor,
      NgIf,
      NgModel,
      SearchInFriends,
      SearchNewFriends,
      MaterialFabComponent,
      MaterialIconComponent,
      MaterialPopupComponent,
      PopupSourceDirective,
      MaterialFabComponent,
    ],
    pipes: [
      commonPipes
    ])
class AddFriendsComponent implements OnActivate {
  User user;
  final UsersService _usersService;
  JsonDecoder jsonDecoder = JsonDecoder();

  // Keep track of each popup's visibility separately.
  final visible = List.filled(2, false);

  final List<RelativePosition> _popupPositions = [
    RelativePosition.OffsetTopRight
  ];
  List<RelativePosition> get popupPositions => _popupPositions;

  AddFriendsComponent(this._usersService);

  @override
  void onActivate(_, RouterState current) async {
    user = User.fromJson(jsonDecoder.convert(window.localStorage['user']));
  }
}

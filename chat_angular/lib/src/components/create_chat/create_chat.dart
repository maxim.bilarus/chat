import 'dart:convert';

import 'package:Chat_angular/src/services/chats_service/chats_service.dart';
import 'package:Chat_angular/src/services/users_service/users_service.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:models/models.dart';
import 'dart:html';

@Component(
  selector: 'create-chat',
  providers: [ClassProvider(ChatsService), ClassProvider(UsersService)],
  directives: [
    MaterialButtonComponent,
    MaterialInputComponent,
    MaterialIconComponent,
    formDirectives
  ],
  templateUrl: 'create_chat.html',
  styleUrls: ['create_chat.css'],
)
class CreateChat implements OnInit {
  String title;
  List<User> members;
  List<User> friends;
  User user;
  Chat chat;
  final JsonDecoder _decoder = JsonDecoder();
  final UsersService _usersService;
  final ChatsService _chatsService;
  CreateChat(this._usersService, this._chatsService);

  @override
  void ngOnInit() async {
    user = await User.fromJson(_decoder.convert(window.localStorage['user']));
    friends = await _usersService.findFriends({}, user.id);
  }

  void createChat() async {
    if (title?.trim()?.isEmpty != null && title?.trim()?.isEmpty != true) {
      chat.title = title;
    }
    if (members?.isEmpty != null && members?.isEmpty != true) {
      chat.members = members;
    }

    await _chatsService.createChat(chat);
  }
}

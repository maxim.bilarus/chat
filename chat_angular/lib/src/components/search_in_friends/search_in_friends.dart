import 'dart:convert';

import 'package:Chat_angular/src/services/users_service/users_service.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:models/models.dart';
import 'dart:html';

@Component(
  selector: 'search-in-friends',
  templateUrl: 'search_in_friends.html',
  styleUrls: ['search_in_friends.css'],
  directives: [
    materialInputDirectives,
    MaterialButtonComponent,
    NgModel,
    MaterialIconComponent,
    NgIf,
    NgFor,
    MaterialPopupComponent,
    MaterialTooltipDirective,
    PopupSourceDirective,
    DeferredContentDirective,
    MaterialChipComponent,
    MaterialChipsComponent,
    displayNameRendererDirective,
  ],
  pipes: [commonPipes],
  providers: [popupBindings],
)
class SearchInFriends implements OnInit {
  String searchString;
  String searchMessage;
  User user;
  bool inited = false;
  final visible = List.filled(2, false);

  final List<RelativePosition> _popupPositions = [
    RelativePosition.InlineTopLeft
  ];

  List<RelativePosition> get popupPositions => _popupPositions;
  String minYearsOld;
  String maxYearsOld;
  String patronymic;
  List<Chip> chips = <Chip>[];
  final _jsonDecoder = JsonDecoder();

  final UsersService _usersService;

  SearchInFriends(this._usersService);
  List<User> friends;
  List<User> allFriends;
  @override
  void ngOnInit() async {
    user = User.fromJson(_jsonDecoder.convert(window.localStorage['user']));
    friends = await _usersService.findFriends({}, user.id);
    allFriends = friends;
    if (friends.isEmpty) {
      searchMessage = 'Your friends list is empty.';
    } else {
      searchMessage = 'Your friends:';
    }
  }

  void find() async {
    dynamic queryParams = <String, dynamic>{};
    if ((searchString?.trim()?.isEmpty == true ||
            searchString?.trim()?.isEmpty == null) &&
        (patronymic?.trim()?.isEmpty == true ||
            patronymic?.trim()?.isEmpty == null) &&
        (minYearsOld?.trim()?.isEmpty == true ||
            minYearsOld?.trim()?.isEmpty == null) &&
        (maxYearsOld?.trim()?.isEmpty == true ||
            maxYearsOld?.trim()?.isEmpty == null)) {
      friends = await _usersService.findFriends({}, user.id)
        ..removeWhere((f) => f.id == user.id);
      if (friends.isEmpty) {
        searchMessage = 'Your friends list is empty.';
      } else {
        searchMessage = 'Your friends:';
      }
    } else {
      if (maxYearsOld?.trim()?.isEmpty != null &&
          maxYearsOld?.trim()?.isEmpty != true) {
        queryParams.addAll({'yearsOldEnd': maxYearsOld});
      }
      if (minYearsOld?.trim()?.isEmpty != null &&
          minYearsOld?.trim()?.isEmpty != true) {
        queryParams.addAll({'yearsOldStart': minYearsOld});
      }
      if (patronymic?.trim()?.isEmpty != null &&
          patronymic?.trim()?.isEmpty != true) {
        queryParams.addAll({'patronymic': patronymic});
      }
      if (searchString?.trim()?.isEmpty != null &&
          searchString?.trim()?.isEmpty != true) {
        queryParams.addAll({'searchString': searchString});
      }

      friends = (await _usersService.findFriends(queryParams, user.id));
      if (friends.isEmpty && allFriends.isNotEmpty) {
        searchMessage = 'Friends by this query not found.';
      } else if (friends.isEmpty && allFriends.isNotEmpty) {
        searchMessage = 'Your friends list is empty.';
      } else {
        searchMessage = 'By query finded ${friends.length} friend(s):';
      }
    }
  }

  void deleteFromFriends(User friend) async {
    await _usersService.deleteFriend(user, friend);
  }

  void saveSearchParams() async {
    chips = [];
    if (minYearsOld?.trim()?.isEmpty != true &&
        minYearsOld?.trim()?.isEmpty != null) {
      chips.add(Chip('minYearsOld: ${minYearsOld.toString()}'));
    }
    if (maxYearsOld?.trim()?.isEmpty != true &&
        maxYearsOld?.trim()?.isEmpty != null) {
      chips.add(Chip('maxYearsOld: ${maxYearsOld.toString()}'));
    }
    if (patronymic?.trim()?.isEmpty != true &&
        patronymic?.trim()?.isEmpty != null) {
      chips.add(Chip('patronymic: ${patronymic.toString()}'));
    }
  }

  void removeChipAndQueryParam(Chip chip) async {
    chips.remove(chip);
    if (chip.uiDisplayName == 'maxYearsOld: ${maxYearsOld.toString()}') {
      maxYearsOld = '';
    }
    if (chip.uiDisplayName == 'minYearsOld: ${minYearsOld.toString()}') {
      minYearsOld = '';
    }
    if (chip.uiDisplayName == 'patronymic: ${patronymic.toString()}') {
      patronymic = '';
    }
  }
}

class Chip implements HasUIDisplayName {
  @override
  final String uiDisplayName;
  const Chip(this.uiDisplayName);
}

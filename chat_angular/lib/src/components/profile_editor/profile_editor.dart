import 'dart:convert';
import 'package:Chat_angular/src/services/users_service/users_service.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_router/angular_router.dart';
import 'package:models/models.dart';
import 'package:Chat_angular/src/routes/routes.dart';
import 'dart:html';

@Component(
    selector: 'profile-editor',
    templateUrl: 'profile_editor.html',
    styleUrls: [
      'profile_editor.css',
    ],
    directives: [
      materialInputDirectives,
      MaterialIconComponent,
      MaterialButtonComponent,
      NgIf,
      MaterialProgressComponent,
      MaterialFabComponent,
      routerDirectives,
      NgFor,
      MaterialDropdownSelectComponent,
      MaterialDatepickerComponent,
      DateRangeInputComponent,
    ],
    exports: [
      RoutePaths,
      Routes
    ],
    providers: [
      datepickerBindings,
    ])
class ProfileEditor implements OnActivate {
  User user;
  User userCopy;
  final Router _router;
  Date date;
  Date birthday;
  DateRange limitToRange =
      DateRange(Date.today().add(years: -113), Date.today().add(years: -13));
  final UsersService _usersService;
  ProfileEditor(this._usersService, this._router);
  final JsonDecoder _jsonDecoder = JsonDecoder();
  final JsonEncoder _jsonEncoder = JsonEncoder();
  @override
  void onActivate(_, RouterState current) async {
    user = User.fromJson(_jsonDecoder.convert(window.localStorage['user']));
    userCopy = User.fromJson(_jsonDecoder.convert(window.localStorage['user']));
    if (user.profile.birthday != null) {
      date = Date(user.profile.birthday.year, user.profile.birthday.month,
          user.profile.birthday.day);
      birthday = Date(user.profile.birthday.year, user.profile.birthday.month,
          user.profile.birthday.day);
    }
  }

  void save() async {
    if (date != null) {
      user.profile.birthday = DateTime(date.year, date.month, date.day);
      birthday = Date(user.profile.birthday.year, user.profile.birthday.month,
          user.profile.birthday.day);
    }
    user = (await _usersService.updateUser(user))
      ..password = null
      ..login = null;
    user.profile.json.values;
    user.profile.json.keys;
    userCopy = (await _usersService.findUser(user.id.value))
      ..password = null
      ..login = null;
    window.localStorage['user'] = _jsonEncoder.convert(userCopy.json);
    await _router.navigate(RoutePaths.profile_viewer.toUrl());
  }
}

import 'dart:convert';

import 'package:Chat_angular/src/services/users_service/users_service.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

import 'package:models/models.dart';
import 'package:angular_router/angular_router.dart';
import 'package:Chat_angular/src/routes/routes.dart';
import 'package:angular_components/material_input/material_input.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'dart:html';

@Component(
    selector: 'register-component',
    templateUrl: 'register_component.html',
    styleUrls: [
      'register_component.css'
    ],
    directives: [
      routerDirectives,
      MaterialInputComponent,
      NgIf,
      MaterialButtonComponent
    ],
    exports: [
      RoutePaths,
      Routes,
    ])
class RegisterComponent {
  final title = 'Registration';
  User user;
  String exceptionMessage;
  final UsersService _usersService;
  final Router _router;
  final JsonEncoder _jsonEncoder = JsonEncoder();

  RegisterComponent(this._usersService, this._router);

  Future<void> register(
      String login, String password, String reEnterPassword) async {
    if (reEnterPassword != password) {
      exceptionMessage = 'Пароль и повтор пароля не совпадают.';
    } else {
      user = await _usersService.registerUser(
          User(login: login, password: password, profile: Profile()));
      window.localStorage['user'] =
          _jsonEncoder.convert((user..password = null).json);
      await _usersService.loginUser(login, password);
      await _router.navigate(RoutePaths.chats.toUrl());
    }
  }

  void loginUrl() => _router.navigate(RoutePaths.login.toUrl());
}

import 'dart:async';
import 'dart:convert';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:models/models.dart';
import 'package:Chat_angular/src/routes/routes.dart';
import 'package:angular_components/material_input/material_input.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_router/angular_router.dart';
import 'dart:html';

@Component(
  selector: 'personal_account',
  templateUrl: 'personal_account.html',
  styleUrls: [
    'package:angular_components/app_layout/layout.scss.css',
    'personal_account.css'
  ],
  providers: [popupBindings],
  directives: [
    DeferredContentDirective,
    MaterialDrawerBase,
    materialInputDirectives,
    MaterialIconComponent,
    MaterialButtonComponent,
    routerDirectives,
    formDirectives,
    MaterialTemporaryDrawerComponent,
    MaterialListComponent,
    MaterialTooltipDirective,
    MaterialListItemComponent,
    MaterialFabComponent
  ],
  exports: [
    RoutePaths,
    Routes,
  ],
)
class PersonalAccount implements OnActivate, DoCheck, OnInit, CanReuse {
  String title;
  User user;
  JsonDecoder jsonDecoder = JsonDecoder();
  final Window w = window;
  String exceptionMessage;
  String message;
  final StreamController<String> _exceptionMessageSpyController =
      StreamController<String>.broadcast();
  final Router _router;
  PersonalAccount(this._router);

  @override
  void onActivate(_, RouterState current) async {
    user =
        await User.fromJson(jsonDecoder.convert(window.localStorage['user']));
    if (current.routePath.path == 'profile/view') {
      title = 'Profile viewing';
    } else if (current.routePath.path == 'chats') {
      title = 'Chats';
    } else if (current.routePath.path == 'friends/add') {
      title = 'Add friends';
    } else if (current.routePath.path == 'profile/edit') {
      title = 'Edit profile';
    }
  }

  @override
  void ngOnInit() async {
    await _exceptionMessageSpyController.stream.listen((exception) async {
      switch (exception) {
        case 'unauthorized':
          await _router.navigate(RoutePaths.login.toUrl());
          window.localStorage.remove('jwt');
          window.localStorage.remove('user');
          break;
      }
    });
  }

  @override
  Future<bool> canReuse(RouterState current, RouterState next) async {
    return true;
  }

  @override
  void ngDoCheck() {
    if (window.localStorage['exceptionMessage'] == 'unauthorized') {
      _exceptionMessageSpyController.add('unauthorized');
    } else if (window.localStorage['jwt'] == null ||
        window.localStorage['user'] == null) {
      _exceptionMessageSpyController.add('unauthorized');
      window.localStorage['exceptionMessage'] = 'unauthorized';
    }
  }
}

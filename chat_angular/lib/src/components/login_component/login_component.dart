import 'dart:async';
import 'dart:convert';
import 'dart:html';

import 'package:Chat_angular/src/services/users_service/users_service.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:models/models.dart';
import 'package:Chat_angular/src/routes/routes.dart';
import 'package:angular_components/material_input/material_input.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_router/angular_router.dart';

@Component(
  selector: 'login-component',
  templateUrl: 'login_component.html',
  styleUrls: [
    'login_component.css',
  ],
  directives: [
    routerDirectives,
    MaterialInputComponent,
    NgIf,
    MaterialButtonComponent,
  ],
  exports: [RoutePaths, Routes],
)
class LoginComponent implements OnActivate {
  final title = 'Authorization';
  dynamic userOrStatusCode;
  String exceptionMessage;
  User user;
  final UsersService _usersService;
  final Router _router;
  final JsonEncoder _jsonEncoder = JsonEncoder();

  LoginComponent(this._usersService, this._router);

  Future<void> login(String login, String password) async {
    if (login.isEmpty || password.isEmpty) {
      exceptionMessage = 'Вы не ввели логин/пароль!';
    } else {
      userOrStatusCode = await _usersService.loginUser(login, password);
      switch (userOrStatusCode) {
        case HttpStatus.unauthorized:
          exceptionMessage = 'Неверный логин или пароль!';
          window.localStorage.remove('exceptionMessage');
          break;
        default:
          switch (userOrStatusCode.runtimeType) {
            case User:
              userOrStatusCode.login = null;
              userOrStatusCode.password = null;
              user = userOrStatusCode;
              window.localStorage['user'] =
                  _jsonEncoder.convert((user..password = null).json);
              await mainUrl();
              break;
            default:
              exceptionMessage = 'Извините сервер временно не работает :(';
              window.localStorage.remove('exceptionMessage');
              break;
          }
          break;
      }
    }
  }

  @override
  void onActivate(_, RouterState current) {
    if (window.localStorage['exceptionMessage'] == 'unauthorized') {
      exceptionMessage =
          'Токен авторизации не действителен, пожалуйста авторизуйтесь еще раз.';
      window.localStorage.remove('exceptionMessage');
    }
  }

  void mainUrl() async =>
      await _router.navigate(RoutePaths.profile_viewer.toUrl());

  void registerUrl() async =>
      await _router.navigate(RoutePaths.register.toUrl());
}

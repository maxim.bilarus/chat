import 'dart:convert';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_router/angular_router.dart';
import 'package:models/models.dart';
import 'package:Chat_angular/src/routes/routes.dart';
import 'dart:html';

@Component(
    selector: 'profile-viewer',
    templateUrl: 'profile_viewer.html',
    styleUrls: [
      'profile_viewer.css',
    ],
    directives: [
      materialInputDirectives,
      MaterialIconComponent,
      MaterialButtonComponent,
      NgIf,
      MaterialProgressComponent,
      MaterialFabComponent,
      routerDirectives,
    ],
    exports: [
      RoutePaths,
      Routes
    ])
class ProfileViewer implements OnActivate {
  User user;
  final JsonDecoder _jsonDecoder = JsonDecoder();

  @override
  void onActivate(_, RouterState current) async {
    user = User.fromJson(_jsonDecoder.convert(window.localStorage['user']));
  }
}

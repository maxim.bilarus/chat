import 'dart:convert';

import 'package:Chat_angular/src/components/create_chat/create_chat.dart';
import 'package:Chat_angular/src/components/search_in_chats/search_in_chats.dart';
import 'package:Chat_angular/src/services/chats_service/chats_service.dart';
import 'package:Chat_angular/src/services/messages_service/messages_service.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angular_router/angular_router.dart';
import 'package:models/models.dart';
import 'dart:html';

@Component(
    selector: 'chats-component',
    templateUrl: 'chats_component.html',
    styleUrls: [
      'chats_component.css'
    ],
    providers: [
      ClassProvider(ChatsService),
      ClassProvider(MessagesService)
    ],
    directives: [
      materialInputDirectives,
      NgFor,
      MaterialIconComponent,
      MaterialButtonComponent,
      NgIf,
      MaterialFabComponent,
      NgModel,
      formDirectives,
      CreateChat,
      SearchInChats,
      MaterialPopupComponent,
      MaterialTooltipDirective,
      PopupSourceDirective,
      DeferredContentDirective,
      MaterialChipComponent,
      MaterialChipsComponent,
      displayNameRendererDirective,
    ])
class ChatsComponent implements OnInit {
  List<Chat> chats;
  List<Message> messages = [];
  User user;
  String searchString;
  String searchMessage;
  final JsonDecoder _jsonDecoder = JsonDecoder();
  final ChatsService _chatsService;
  final MessagesService _messagesService;

  ChatsComponent(this._chatsService, this._messagesService);
  final visible = List.filled(1, false);

  final List<RelativePosition> _popupPositions = [
    RelativePosition.OffsetTopRight
  ];

  List<RelativePosition> get popupPositions => _popupPositions;
  @override
  void ngOnInit() async {
    user = User.fromJson(_jsonDecoder.convert(window.localStorage['user']));
    chats = await _chatsService.find({'memberId': user.id.value});
  }

  void find(String searchString) {}
}

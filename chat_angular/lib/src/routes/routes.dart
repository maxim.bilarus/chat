import 'package:angular_router/angular_router.dart';
import 'package:Chat_angular/src/components/login_component/login_component.template.dart'
    as login_template;
import 'package:Chat_angular/src/components/register_component/register_component.template.dart'
    as register_template;
import 'package:Chat_angular/src/components/personal_account/personal_account.template.dart'
    as personal_account_template;
import 'package:Chat_angular/src/components/profile_viewer/profile_viewer.template.dart'
    as profile_viewer_template;
import 'package:Chat_angular/src/components/profile_editor/profile_editor.template.dart'
    as profile_editor_template;
import 'package:Chat_angular/src/components/chats_component/chats_component.template.dart'
    as chats_template;
import 'package:Chat_angular/src/components/add_friends_component/add_friends_component.template.dart'
    as friends_template;
import 'package:Chat_angular/src/route_paths/route_paths.dart';
export 'package:Chat_angular/src/route_paths/route_paths.dart';

class Routes {
  static final login = RouteDefinition(
    routePath: RoutePaths.login,
    component: login_template.LoginComponentNgFactory,
  );
  static final register = RouteDefinition(
      routePath: RoutePaths.register,
      component: register_template.RegisterComponentNgFactory);
  static final main = RouteDefinition(
    routePath: RoutePaths.personal_account,
    component: personal_account_template.PersonalAccountNgFactory,
  );
  static final profile_viewer = RouteDefinition(
    routePath: RoutePaths.profile_viewer,
    component: profile_viewer_template.ProfileViewerNgFactory,
  );
  static final chats = RouteDefinition(
    routePath: RoutePaths.chats,
    component: chats_template.ChatsComponentNgFactory,
  );
  static final add_friends = RouteDefinition(
    routePath: RoutePaths.add_friends,
    component: friends_template.AddFriendsComponentNgFactory,
  );
  static final profile_editor = RouteDefinition(
    routePath: RoutePaths.profile_editor,
    component: profile_editor_template.ProfileEditorNgFactory,
  );

  static final app = <RouteDefinition>[
    RouteDefinition.redirect(path: '', redirectTo: Routes.login.toUrl()),
    login,
    register,
    main,
  ];

  static final account = <RouteDefinition>[
    profile_viewer,
    add_friends,
    chats,
    profile_editor
  ];
}

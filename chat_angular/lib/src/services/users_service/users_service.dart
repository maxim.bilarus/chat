import 'package:models/models.dart';
import 'package:client/client.dart';

class UsersService {
  Future<dynamic> loginUser(String login, String password) async =>
      await users.login(login, password);

  Future<List<User>> findFriends(Map<String, dynamic> query, UserId id) async =>
      await users.readFriends(id, query);

  Future<List<User>> find(Map<String, dynamic> query) async =>
      await users.read(query);

  Future<User> registerUser(User user) async => await users.create(user);

  Future<User> findUser(String id) async => await users.read(UserId(id));

  Future<User> updateUser(User user) async => await users.update(user);

  Future<List<User>> deleteFriend(User user, User friend) async =>
      await users.deleteFriend(user, friend);
}

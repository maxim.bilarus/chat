import 'package:models/models.dart';
import 'package:client/client.dart';

class MessagesService {
  Future<List<Message>> findMessage(Map<String, dynamic> query) async =>
      await messages.read(query);

  Future<Message> sendMessage(Message message) async =>
      await messages.create(message);

  Future<Message> updateMessage(Message message) async =>
      await messages.update(message);

  Future<List<Message>> deleteMessage(Message message) async =>
      await messages.delete(message);
}

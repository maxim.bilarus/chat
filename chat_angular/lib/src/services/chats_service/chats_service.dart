import 'package:models/models.dart';
import 'package:client/client.dart';

class ChatsService {
  Future<List<Chat>> findChats(Map<String, dynamic> query, UserId id) async =>
      await chats.getByContext();

  Future<List<Chat>> find(Map<String, String> query) async =>
      await chats.read(query);

  Future<List<Chat>> getByContext() async => await chats.getByContext();

  Future<Chat> createChat(Chat chat) async => await chats.create(chat);

  Future<Chat> findChat(String id) async => await chats.read(ChatId(id));

  Future<Chat> updateChat(Chat chat) async => await chats.update(chat);
}

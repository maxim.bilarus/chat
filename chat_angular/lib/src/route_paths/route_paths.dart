import 'package:angular_router/angular_router.dart';

class RoutePaths {
  static final login = RoutePath(path: 'login/');
  static final register = RoutePath(path: 'register/');
  static final personal_account = RoutePath(path: '');
  static final profile_viewer =
      RoutePath(path: 'profile/view', parent: personal_account);
  static final profile_editor =
      RoutePath(path: 'profile/edit', parent: personal_account);
  static final chats = RoutePath(path: 'chats', parent: personal_account);
  static final add_friends =
      RoutePath(path: 'friends/add', parent: personal_account);
}

import 'package:angular/angular.dart';
import 'package:Chat_angular/src/components/app_component/app_component.template.dart'
    as ng;
import 'package:angular_router/angular_router.dart';
import 'main.template.dart' as self;
import 'package:http/browser_client.dart';

@GenerateInjector([routerProvidersHash, ClassProvider(BrowserClient)])
final InjectorFactory injector = self.injector$Injector;

void main() {
  runApp(ng.AppComponentNgFactory, createInjector: injector);
}
